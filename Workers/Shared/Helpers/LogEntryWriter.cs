﻿using System;
using BH.ScalableServices.Brokers.SDKBase;
using BH.ScalableServices.Brokers.Shared.Models;
using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Shared.Helpers
{
    public static class LogEntryWriter
    {
        public static IScalableServiceWebClient ServiceWebClient { get; set; }
        public static void LogMessage(string msg,string runId)
        {
            var logmsg = new ScalableServiceLogEntry();
            logmsg.Message = msg;
            logmsg.Timestamp = DateTime.UtcNow;
            logmsg.Source = runId;
            StatusUpdater.ServiceWebClient.PostLogEntry(logmsg, runId);
        }
        public static void LogObject(object jsonObj, string objectMessage,string runId)
        {
            var jsonTVs = JsonConvert.SerializeObject(jsonObj);
            LogMessage($"{objectMessage}: {jsonTVs}",runId);
        }
    }
}
