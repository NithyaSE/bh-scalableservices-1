﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace BH.ScalableServices.Workers.Shared.CustomExceptions
{/// <summary>
///
/// </summary>

   [Serializable]
    public class MessageProcessingFailedCustomException : Exception
    {
        public string ResourceReferenceProperty { get; set; }
        public MessageProcessingFailedCustomException()
        {
        }

        public MessageProcessingFailedCustomException(string message)
            : base(message)
        {
        }

        public MessageProcessingFailedCustomException(string message, Exception inner)
            : base(message, inner)
        {
        }
        protected MessageProcessingFailedCustomException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            ResourceReferenceProperty = info.GetString("ResourceReferenceProperty");
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");
            info.AddValue("ResourceReferenceProperty", ResourceReferenceProperty);
            base.GetObjectData(info, context);
        }
    }
}


