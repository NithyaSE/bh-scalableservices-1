﻿using System;
using Nancy;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{
    public class WorkflowException : Exception
    {
        public WorkflowResponse Response { get; set; }

        public WorkflowException(WorkflowResponse response) : base(response.Error)
        {
            Response = response;

        }

        public WorkflowException(string message, HttpStatusCode statusCode) : base(message)
        {
            Response = new WorkflowResponse
            {
                Error = message,
                StatusCode = statusCode
            };
        }
    }
}
