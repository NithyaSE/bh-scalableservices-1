﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models
{
    public class ProcessStatus
    {
        [JsonProperty("full")]
        public string Full { get; set; }

        [JsonProperty("current")]
        public string Current { get; set; }

    }
}
