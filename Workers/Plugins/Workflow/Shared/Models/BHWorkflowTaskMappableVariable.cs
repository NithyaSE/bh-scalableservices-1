﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{
    /// <summary>
    /// Represents a task variable that can be mapped to a process variable.
    /// </summary>
    public class BHWorkflowTaskMappableVariable<T>
    {
        [JsonProperty("is_mapped")]
        public bool IsMapped { get; set; }

        [JsonProperty("process_variable")]
        public string ProcessVariable { get; set; }
        [JsonProperty("value")]
        public T Value { get; set; }

        public BHWorkflowTaskMappableVariable(T val)
        {
            Value = val;
        }

    }
}
