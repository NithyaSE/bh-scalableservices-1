﻿using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models
{
    /// <summary>
    /// Represents the type returned by a task when it wants to finish
    /// </summary>
    public class FinishRunTaskReturn :IRunTaskReturn
    {
        public JObject WorkingSet { get; set; }
    }
}
