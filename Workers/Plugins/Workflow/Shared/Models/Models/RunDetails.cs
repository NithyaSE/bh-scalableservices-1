﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Serilog;

namespace BHX.Workflow.Shared.Models.Models
{
    public class RunDetailsV1
    {
        [JsonProperty("run_id")]
        public string RunId { get; set; }

        [JsonProperty("process_instance_name")]
        public string ProcessInstanceName { get; set; }

        [JsonProperty("total_run_time_ms")]
        public double TotalRunTimeMS { get; set; }

        [JsonProperty("log_entries")]
        public List<LogEntryV1> LogEntries { get; set; }

        [JsonProperty("is_running")]
        public bool IsRunning { get; set; }

        [JsonProperty("is_error_path")]
        public bool IsErrorPath { get; set; }

        [JsonProperty("is_unhandeled_exception")]
        public bool IsUnhandeledException { get; set; }

        [JsonIgnore]
        public DateTimeOffset RunStartTime { get; set; }

        [JsonIgnore]
        public ProcessInstance ProcessInstance { get; set; }

        [JsonIgnore]
        public ILogger Logger { get; set; }

        private WorkflowSettings _settings;

        public RunDetailsV1(ILogger logger,WorkflowSettings settings):this()
        {
            Logger = logger;
            _settings = settings;
        }

        public RunDetailsV1()
        {
            LogEntries = new List<LogEntryV1>();
        }


        /// <summary>
        /// Logs the details about a running process
        /// </summary>
        /// <param name="msg">Message to be logged</param>
        public void Log(string msg, ProcessLogType type = ProcessLogType.Info)
        {

            var timestamp = DateTime.Now;
            var message = string.Format("{0} : {1} : {2} (Process) : {3}", type, RunId, ProcessInstance.Name, msg);

           // LogLocal(timestamp, message);
            LogEntries.Add(new LogEntryV1
            {
                Timestamp = timestamp,
                Entry = message
            });
            
        }

        //private void LogLocal(DateTime timestamp, string msg)
        //{
        //    msg = string.Format("[{0}] : {1}", timestamp, msg);
        //    Console.WriteLine(msg);
        //    var logDirectory = Path.Combine(_settings.ProcessRunsLocation ,RunId)+ "\\Log";
        //    if (!Directory.Exists(logDirectory)) Directory.CreateDirectory(logDirectory);

        //    var dirInfo = new DirectoryInfo(logDirectory);
        //    var fileInfo = new FileInfo(dirInfo.FullName + "\\RunTimeLog.log");
        //    Logger.WriteToCustomLog(msg, dirInfo, fileInfo);

        //}
        
    }
    
}
