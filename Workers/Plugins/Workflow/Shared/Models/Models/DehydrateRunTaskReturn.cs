﻿using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{
    /// <summary>
    /// Represents the type returned by a task when it wants to dehydrate
    /// </summary>
    public class DehydrateRunTaskReturn :IRunTaskReturn
    {
        public JObject WorkingSet { get; set; }
        public string CheckInterval { get; set; }
    }
}
