﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{
    public class KeyValueObject<T>
    {
        [JsonProperty("key")]
        public string Key { get; set; }
        [JsonProperty("value")]
        public T Value { get; set; }
    }
}
