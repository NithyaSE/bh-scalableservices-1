﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{
    public class SystemTaskVariables
    {
        [JsonProperty("$run_id")]
        public string ProcessRunId { get; set; }
        [JsonProperty("$workflow_task_instance_name")]
        public string WorkflowTaskInstanceName { get; set; }
        [JsonProperty("$is_rehydrated")]
        public bool IsRehydrated { get; set; }
    }
}
