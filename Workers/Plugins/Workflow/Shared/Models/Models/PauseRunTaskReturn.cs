﻿using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{ 
    /// <summary>
    /// Represents the type returned by a task when it wants to pause
    /// </summary>
    public class PauseRunTaskReturn : IRunTaskReturn
    {
        public JObject WorkingSet { get; set; }
        public string ResumeToken { get; set; }
    }
}
