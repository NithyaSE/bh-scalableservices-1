﻿using System;
using System.Linq;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class WorkflowTask : IWorkflowTask
    {
        /// <summary>
        /// Workflow Settings that come from the main settings configuration file
        /// </summary>
        public WorkflowSettings Settings { get; set; }
        public SystemTaskVariables SystemTaskVariables { get; set; }
        private JObject _systemTaskVariablesJObject;
        public void SetSystemTaskVariablesJObject(JObject systemTaskVariables)
        {
            _systemTaskVariablesJObject = systemTaskVariables;
            SystemTaskVariables = new SystemTaskVariables
            {
                ProcessRunId = _systemTaskVariablesJObject["$run_id"]?.ToString(),
                WorkflowTaskInstanceName = _systemTaskVariablesJObject["$workflow_task_instance_name"]?.ToString(),
                IsRehydrated = bool.Parse(_systemTaskVariablesJObject["$is_rehydrated"].ToString())
            };
        }
        public abstract IRunTaskReturn RunTask(JObject workingSet);
        /// <summary>
        /// Sets the required PVs on a sub process instance by reading the required pvs from system task variables.
        /// </summary>
        /// <param name="subProcressInstance"></param>
        protected void SetRequiredPVsOnSubProcessInstance(WorkflowProcessInstance subProcressInstance)
        {
            var masterProcessRequiredPvs = _systemTaskVariablesJObject?.Properties().Where(p => p.Name.StartsWith("!")).ToDictionary(p => p.Name, p => p.Value);
            var subprocessRequiredPvs = subProcressInstance.ProcessVariables.Where(pv => pv.Key.StartsWith("!")).ToDictionary(pv => pv.Key, pv => pv.Value);
            var conbinesRequiredPvList = masterProcessRequiredPvs.Keys.Union(subprocessRequiredPvs.Keys).Distinct();
            foreach (var requiredPvName in conbinesRequiredPvList)
            {
                if (masterProcessRequiredPvs.ContainsKey(requiredPvName))
                {
                    if (subprocessRequiredPvs.ContainsKey(requiredPvName))
                    {
                        var masterRequiredPvVal = masterProcessRequiredPvs[requiredPvName];
                        subProcressInstance.ProcessVariables[requiredPvName] = masterRequiredPvVal;
                    }
                    else
                    {
                        throw new Exception($"No required PV found in the sub process for master process required PV '{requiredPvName}'");
                    }
                }
                else
                {
                    throw new Exception($"No required PV found in the master process for sub process required PV '{requiredPvName}'");
                }
            }
        }
    }
}
