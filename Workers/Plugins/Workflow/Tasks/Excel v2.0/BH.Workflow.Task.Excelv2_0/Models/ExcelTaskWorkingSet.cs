﻿using System.Collections.Generic;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.ExcelWithParameters.v2_0.Models
{
    public class ExcelTaskWorkingSet
    {
        [JsonProperty("file_name")]
        public string FileName { get; set; }
        [JsonProperty("macro_name")]
        public string MacroName { get; set; }
        [JsonProperty("is_visible")]
        public bool IsVisible { get; set; }
        [JsonProperty("timeout")]
        public string Timeout { get; set; }
        [JsonProperty("excel_parameters")]
        public List<KeyValueObject<object>> ExcelParameters {get;set;}
    }
}
