﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.ExcelWithParameters.v2_0
{
    public class ExcelOutputVariable
    {
        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("is_mapped")]
        public bool isMapped { get; set; }

        [JsonProperty("process_variable")]
        public string ProcessVariable { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

    }
}
