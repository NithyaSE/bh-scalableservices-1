﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Shared.SerilogsLogging;

namespace BH.Excel.Automation
{
    public class ExcelAPI : IExcelAPI
    {
        [DllImport("user32.dll")]
        private static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        public Microsoft.Office.Interop.Excel.Application xlApp;
        private uint? ProcessID;
        Microsoft.Office.Interop.Excel.Workbook xlWorkBook = null;
        // protected readonly ManualResetEvent StopEvent = new ManualResetEvent(false);
        private int timeout = 900;
        private string _filename;
        private string _macroname;
        private bool _timeoutReached = false;
        private bool IsVisible = false;
        public ExcelAPI()
        {
            xlApp = new Microsoft.Office.Interop.Excel.Application {DisplayAlerts = false};
            xlApp.Parent.DisplayAlerts = false;
            xlApp.Visible = true;
        }

        public ExcelAPI(bool isVisible)
        {
            xlApp = new Microsoft.Office.Interop.Excel.Application {DisplayAlerts = false};
            xlApp.Parent.DisplayAlerts = false;
            xlApp.Visible = isVisible;
        }

        public void Dispose()
        {
            try
            {
                if (xlApp == null) return;
                Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.Dispose()' Summary='Attempting to close the excel application.'");
                Serilog.Log.Logger.AddMethodName().Debug("Summary='Attempting to Dispose the Excel App Object before the garbage collection'");
                xlApp.Quit();
            }
            catch (Exception ex)
            {
                if (_timeoutReached)
                    throw new TimeoutException("The excel timed out and the excel process was killed causing the application to lose the excel handle. The Task is unable to communicate with the Excel instance." ,ex);
                else
                // absorb the exception here
                    Serilog.Log.Logger.AddMethodName().Error("Summary='An excel exception has occurred causing the Task to be unable to communicate with the Excel instance. The Excel instance may be hung and blocking the Workflow Worker' Details='{@ex}'" , ex);
            }
            finally
            {
                Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.Dispose()-finally' Summary='Releasing the excel app.'");
                releaseObject(xlApp);
            }
        }

        public void SetIsVisible(bool isVisible)
        {
            xlApp.Visible = isVisible;
        }


        /// <summary>
        /// Calls an Excel Function using a scripting Dictionary for the inputs and outputs.
        /// 
        /// Note: Function must exist in Excel Modules for a response that is not null
        /// </summary>
        /// <param name="filename">Full file location of the excel file</param>
        /// <param name="macroName">Name of the macro to run</param>
        /// <param name="parameters">Dictionary to provide to excel function as a parameter</param>
        /// <param name="parameters2">Dictionary to provide to excel function as a second parameter</param>
        /// <param name="numberOfAttempts">Number of attempts to open and run the macro.  -1 will force it to try indefinitely</param>
        /// <param name="save">Whether to save the excel sheet at the end of processing or not</param>
        /// <returns></returns>
        public Scripting.Dictionary CallExcelMacroWithDictionary(string filename, string macroName, Scripting.Dictionary parameters, int numberOfAttempts = 20, bool save = true, int timeoutSeconds = 900) // numberofattempts -1 for infinite tries  // add on save or not
        {
            try
            {
                Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.CallExcelMacroWithDictionary()' Summary='Trying to save the process ID for killing the process at the end'");
                ProcessID = SaveProcessID();
                Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.CallExcelMacroWithDictionary()' Summary='Saved the process ID {ProcessID} to kill the excel at the end of task'");
                
            }
            catch (Exception exp)
            {
                Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.CallExcelMacroWithDictionary()-catch' Summary='Failed to get the process ID with exception {exp.Message}' Details='{exp.StackTrace}'");
            }
            //TODO: Do we still want to continue if the process id cannot be retrieved??

            Exception e = null;
            timeout = timeoutSeconds;
            Scripting.Dictionary result = null;
            var again = true;
            _filename = filename;
            _macroname = macroName;

            try
            {
                //~~> Start Excel and open the workbook.
                // start a task with a means to do a hard abort (unsafe!)
                CancellationTokenSource Canceller = new CancellationTokenSource();
                Task<Scripting.Dictionary> excelAPICallTask = Task.Factory.StartNew(() => ExecuteExcel(filename, numberOfAttempts, macroName, parameters, save, ref again, out e, Canceller.Token), Canceller.Token);
                Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.CallExcelMacroWithDictionary()' Summary='Starting to execute excel");
                var completed = excelAPICallTask.Wait(timeout * 1000);
                if (!completed)
                {
                    Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.CallExcelMacroWithDictionary()' Summary='The timer went off!. Cancelling the task.");
                    Canceller.Cancel();
                    _timeoutReached = true;
                    throw new TimeoutException("Excel timed out at " + timeout + " seconds.  Ensure excel macro is running properly.  Filename: " + _filename + "  Macro: " + _macroname);
                }
                else
                {
                    if (again)
                        throw new Exception("Excel Macro Failed To Run For FileName : " + filename + " : Macro Name : " + macroName, e);
                    result = excelAPICallTask.Result;
                }
            }
            finally
            {
                //cleanup
                 if (!_timeoutReached) 
                {
                    try
                    {
                        releaseObject(xlWorkBook);
                    }
                    catch(Exception exp)
                    {
                        Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.CallExcelMacroWithDictionary()-finally catch' Summary='Error Closing workbook or excelApp programatically. {exp.Message}");
                    }
                    
                }

            }
            
            return result;
        }

        private Scripting.Dictionary ExecuteExcel(string filename,int numberOfAttempts,string macroName,Scripting.Dictionary parameters,bool save,ref bool again,out Exception ex,CancellationToken token)
        {
            Scripting.Dictionary result = null;
            ex = null;
             using (token.Register(() => {
                 //Code to be executed on timeout
                 try
                 {
                     Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.ExecuteExcel()-Timeout' Summary='Running Cleanup code after timeout..");
                      //TODO: need to close the workbook or else the temp file cannot be deleted until the appdomain releases all resources.
                     Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.ExecuteExcel()-Timeout' Summary='killing the excel process without making any attempts at closing the excel.");
                     TryKillingExcelProcess();
                     xlApp = null;
                    
                 }
                 catch (Exception exp)
                 {
                     Console.WriteLine(
                         $"{DateTime.Now:s} Method : 'ExcelAPI.ExecuteExcel()-Timeout catch' Summary='Excel process could not be killed.. {exp.Message}");
                 }
                 
              
             }))
                {
                xlWorkBook = xlApp.Workbooks.Open(filename);
                var attempts = 0;
                    while (again && (attempts++ < numberOfAttempts || numberOfAttempts == -1) && !_timeoutReached) // && timeout met
                    {
                        try
                        {
                            Serilog.Log.Logger.AddMethodName().Information("Summary='Running excel for attempt - {@no}'", attempts);
                            xlApp.Visible = false;
                            //~~> Run the macros by supplying the necessary arguments
                            result = xlApp.Run(macroName, parameters);
                            again = false;
                            Serilog.Log.Logger.AddMethodName().Information("Summary='Ran excel successfully'", attempts);
                    }
                    catch (Exception exc)
                        {
                            Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.ExecuteExcel()-while loop catch' Summary='Exception occured in the while loop executing the excel. {exc.Message}'");
                            Serilog.Log.Logger.AddMethodName().Warning("Summary='Excel run failed in attempt '{@attempt}' with exception '{@excMessage}' ' Details='{@exp}'", attempts, exc.Message, exc);
                            ex = exc;
                        }
                        finally
                        {
                            Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.ExecuteExcel()-while loop finally' Summary='Attempting to close the workbook in finally block from inside the thread executing the excel..");
                            Serilog.Log.Logger.AddMethodName().Information("Summary='Attempting to close the workbook in finally block from inside the thread executing the excel..'");
                      
                        try
                        {
                            if (xlWorkBook != null)
                            {
                                if (save)
                                    xlWorkBook.Save();
                                xlWorkBook.Close();
                            }
                        }
                        catch(Exception exp)
                        {
                            Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.ExecuteExcel()-while loop finally catch' Summary='Could not close the workbook in the thread running the excel. It might be already closed." +exp.Message);
                        }
                        }

                    }
                }
            return result;
        }


        static void releaseObject(object obj)
        {
            try
            {
                Serilog.Log.Logger.AddMethodName().Debug("Attemting to release the interop resources.");
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception)
            {
                obj = null;
                Serilog.Log.Logger.AddMethodName().Error("Attempt to release the interop resources failed. This could have been caused by a possible timeout'");
            }
            finally
            {
                GC.Collect();
            }
        }
        

        private uint SaveProcessID()
        {
            var k = xlApp.Application.Hwnd;
            uint processID;
            Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.SaveProcessID' Summary='Attempting to get the process ID");
            Serilog.Log.Logger.AddMethodName().Debug("Summary='Attempting to get the process ID of the excel instance for killing the process running excel.'");
            GetWindowThreadProcessId((IntPtr)k, out processID);
            Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.SaveProcessID' Summary='Got the process ID for excel task - {processID}");
            Serilog.Log.Logger.AddMethodName().Debug("Summary='Got the process ID for excel task - {pid}'",processID);
            return processID;
        }

        public void TryKillingExcelProcess()
        {
            try
            {
                if (ProcessID != null && ProcessID.HasValue)
                {
                    var p = Process.GetProcessById((int)ProcessID);
                    Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.TryKillingExcelProcess' Summary='Attempting to kill the excel process with process ID {(int)ProcessID}'" );
                    Serilog.Log.Logger.AddMethodName().Information("Summary='Attempting to kill the excel process with process ID {@id}'", (int)ProcessID);
                    p.Kill();// kills dialog box
                    Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.TryKillingExcelProcess' Summary='Killed the excel process with process ID {(int)ProcessID}'");
                    Serilog.Log.Logger.AddMethodName().Information("Summary='Killed the excel process with process ID {@id}'", (int)ProcessID);
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine($"{DateTime.Now:s} Method : 'ExcelAPI.TryKillingExcelProcess-catch' Summary='Failed to kill th excel process ID {ProcessID}. Exception {exp.Message}'");
                Serilog.Log.Logger.AddMethodName().Error("Summary='Failed to kill th excel process ID {ProcessID}. Exception {@expMessafe}' Details='{@exp}'",ProcessID, exp.Message,exp);
            }
        }



    }





}
