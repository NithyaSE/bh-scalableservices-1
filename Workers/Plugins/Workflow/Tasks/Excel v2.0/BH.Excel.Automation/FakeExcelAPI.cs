﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scripting;

namespace BH.Excel.Automation
{
    public class FakeExcelAPI : IExcelAPI
    {
        public bool IsVisible { get; set; }

        public Dictionary Parameters { get; set; }

        public string MacroName { get; set; }

        public string FileName { get; set; }

        public bool Save { get; set; }

        public int NumberOfAttempts { get; set; }

        public int TimeoutSeconds { get; set; }

        public void Dispose()
        {
        }

        public Dictionary CallExcelMacroWithDictionary(string filename, string macroName, Dictionary parameters,
            int numberOfAttempts = 20, bool save = true, int timeoutSeconds = 900)
        {
            FileName = filename;
            MacroName = macroName;
            Parameters = parameters;
            NumberOfAttempts = numberOfAttempts;
            Save = save;
            TimeoutSeconds = timeoutSeconds;


            return parameters;
        }

        public void SetIsVisible(bool isVisible)
        {
            IsVisible = isVisible;
        }

        public void TryKillingExcelProcess()
        {
            throw new NotImplementedException();
        }
        
    }
}
