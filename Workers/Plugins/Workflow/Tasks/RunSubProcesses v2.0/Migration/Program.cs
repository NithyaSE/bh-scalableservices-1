﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Migration
{
    class Program
    {
        private static string _schemaLocation => ConfigurationManager.AppSettings["SchemaLocation"];
        private static string _processBundlesLibraryLocation => ConfigurationManager.AppSettings["ProcessBundlesLibraryLocation"];
        private static string _oldDllName => "BH.Workflow.Task.RunSubProcess.v2_0";
        private static string _newDllName => "BH.ScalableServices.Workers.Plugins.Workflow.Tasks.RunSubProcess.v2_0.dll";
        static void Main(string[] args)
        {
          UpdateTasks();
          UpdateSchemas();
            Console.ReadLine();
        }

        private static void UpdateTasks()
        {
            //find all files that have the right extension for 
            var subProcInstances = GetAllTasksForDllName(_oldDllName);
            //foreach file
            foreach (var item in subProcInstances)
            {
                var file = item.Key;
                Console.WriteLine($"Updating task {file}");
                var jTokTask = item.Value;
                var processInstanceFilePath = Path.Combine(Path.GetDirectoryName(file), "processinstance.json");
                var processinstanceJson = File.ReadAllText(processInstanceFilePath);
                var processinstance = JsonConvert.DeserializeObject<JToken>(processinstanceJson);
                jTokTask["dll_name"] = _newDllName;
                if (jTokTask["working_set_input_mappings_to_process_variables"] == null)
                {
                    //update timeout
                    UpdatePropertyName(jTokTask, "inputs", "working_set_input_mappings_to_process_variables");
                    var inputsOld = jTokTask["working_set_input_mappings_to_process_variables"];
                    if (inputsOld != null)
                    {
                        // update sub process name property
                        UpdatePropertyName(inputsOld, "base_process_bundle_name", "sub_process_process_name");
                        UpdatePropertyName(inputsOld, "inputs", "sub_process_process_variable_mappings");
                        var subProcInputs = inputsOld["sub_process_process_variable_mappings"];
                        if (subProcInputs["is_mapped"] != null && subProcInputs["is_mapped"].Value<bool>()) // if value mapped
                        {
                            Console.WriteLine(
                                $"The subprocess mappings for Task '{file}' are mapped to PV {subProcInputs["process_variable"]}. Please update them manually.");
                        }
                        else
                        {
                            var mappings = subProcInputs["value"];
                            if (mappings != null && mappings is JArray)
                            {
                                foreach (var mapping in (JArray)mappings)
                                {
                                    var iterations = mapping["value"];
                                    if (!(iterations is JArray)) continue;
                                    foreach (var iteration in (JArray)iterations)
                                    {
                                        UpdateValueToBhMappable(iteration);
                                    }
                                }
                            }
                        }
                    }
                    // Update outputs
                    UpdatePropertyName(jTokTask, "outputs", "working_set_output_mappings_to_process_variables");
                    var outputs = jTokTask["working_set_output_mappings_to_process_variables"];
                    if (outputs == null) continue;
                    var pvToSpvMappings = outputs["pv_to_sub_pv_output_mapping"];
                    if (pvToSpvMappings != null)
                    {
                        var value = pvToSpvMappings["value"];
                        if (pvToSpvMappings["is_mapped"].Value<bool>())
                        {
                            // get the value from process
                            value = processinstance["process_variables"][pvToSpvMappings["process_variable"]];
                        }
                        // 
                        if (value != null && value is JArray)
                        {
                            foreach (var mapping in (JArray)value)
                            {
                                if (mapping != null)
                                {
                                    outputs[mapping["value"].ToString()] = mapping["key"];
                                }
                            }
                        }
                    }
                ((JObject)outputs).Remove("pv_to_sub_pv_output_mapping");
                }
                File.Delete(file);
                File.WriteAllText(file,JsonConvert.SerializeObject(jTokTask));
            }
        }
        private static void UpdateValueToBhMappable(JToken element)
        {
          element["value"] = new JObject { { "is_mapped", element["is_mapped"] } ,
              { "process_variable", element["process_variable"] },
              { "value" , element["value"]}

          } ;
          ((JObject)element).Remove("is_mapped");
          ((JObject)element).Remove("process_variable");
        }

        private static void UpdateSchemaPropertyToBhMappable(JToken element)
        {
            element["value"] =
                JObject.Parse("{\r\n  \"title\": \"Iteration Value\",\r\n  \"description\": \"Value of Subprocess PV for a given iteration\",\r\n  \"type\": \"object\",\r\n  \"format\": \"table\",\r\n  \"properties\": {\r\n    \"is_mapped\": {\r\n      \"title\": \"Is Mapped\",\r\n      \"type\": \"boolean\",\r\n      \"default\": false\r\n    },\r\n    \"process_variable\": {\r\n      \"title\": \"Master Process Process Variable\",\r\n      \"type\": \"string\"\r\n    },\r\n    \"value\": {\r\n      \"title\": \"Value\"\r\n    }\r\n  }\r\n}");
            ((JObject)element).Remove("is_mapped");
            ((JObject)element).Remove("process_variable");
            }
        private static void UpdatePropertyName(JToken parent, string oldPropertyName, string newPropName)
        {
            var oldProperty = parent[oldPropertyName];
            parent[newPropName] = oldProperty;
            ((JObject)parent).Remove(oldPropertyName);
        }
        private static void UpdateSchemas()
        {
            // Update Schemas
            var schemas = GetAllSchemasForDllName(_oldDllName);
            if (schemas == null) return;
            foreach (var inst in schemas)
            {
                var instance = inst.Value;
                var filePath = inst.Key;
                instance["properties"]["dll_name"]["default"] = _newDllName;
                if (instance["dll_name"] != null)
                {
                    instance["dll_name"] = _newDllName;
                }
                if (instance["properties"]?["working_set_input_mappings_to_process_variables"] == null)
                {
                    UpdatePropertyName(instance["properties"], "inputs", "working_set_input_mappings_to_process_variables");
                    instance["properties"]["working_set_input_mappings_to_process_variables"]["description"] = "Working Set Input mappings to Process Variables";
                    instance["properties"]["working_set_input_mappings_to_process_variables"]["title"] =
                        "Working Set Input Mappings";
                    UpdatePropertyName(instance["properties"]["working_set_input_mappings_to_process_variables"]["properties"], "base_process_bundle_name", "sub_process_process_name");
                    UpdatePropertyName(instance["properties"]["working_set_input_mappings_to_process_variables"]["properties"], "inputs", "sub_process_process_variable_mappings");

                    UpdateSchemaPropertyToBhMappable(instance["properties"]?["working_set_input_mappings_to_process_variables"]?["properties"]?["sub_process_process_variable_mappings"]?["properties"]?["value"]?["items"]?["properties"]?["value"]?["items"]?["properties"]);

                    UpdatePropertyName(instance["properties"], "outputs", "working_set_output_mappings_to_process_variables");
                    instance["properties"]["working_set_output_mappings_to_process_variables"]["description"] = "Working Set Output Mappings";
                    instance["properties"]["working_set_output_mappings_to_process_variables"]["title"] =
                        "Working Set Output mappings to Process Variables";
                    var mappingPropDefault = instance["properties"]?["working_set_output_mappings_to_process_variables"]?["properties"]?["pv_to_sub_pv_output_mapping"]?["properties"]?["value"]?["properties"]?["default"];
                    if (mappingPropDefault != null && mappingPropDefault is JArray)
                    {
                        foreach (var mappingDefault in (JArray)mappingPropDefault)
                        {
                            if (mappingDefault != null)
                            {
                                instance["properties"]["working_set_output_mappings_to_process_variables"]["properties"][
                                    mappingDefault["value"].ToString()] = new JObject { { "type", "string" }, { "default", mappingDefault["key"] } };
                            }
                        }
                    }
                    var jObject = (JObject)instance["properties"]?["working_set_output_mappings_to_process_variables"]?["properties"];
                    jObject?.Remove("pv_to_sub_pv_output_mapping");
                    Console.WriteLine($"Updating Schema {filePath}");
                }
                File.Delete(filePath);
                File.WriteAllText(filePath, instance.ToString());
            }
        }
        private static List<string> GetAllFilesUnderLocation(string location)
        {
            var files = new List<string>();
            files.AddRange(Directory.GetFiles(location));
            var dirs = Directory.GetDirectories(location);
            foreach (var processDir in dirs)
            {
                files.AddRange(Directory.GetFiles(processDir, "*", SearchOption.AllDirectories));

            }
            return files;
        }
        private static Dictionary<string, JToken> GetAllSchemasForDllName(string dllName)
        {
            var instances = new Dictionary<string, JToken>();

            var files = GetAllFilesUnderLocation(_schemaLocation);
            foreach (var file in files)
            {
                if (file != null && file.ToLower().EndsWith("_schema.json"))
                {
                    var instanceJson = File.ReadAllText(file);
                    var instance = JsonConvert.DeserializeObject<JToken>(instanceJson);
                    if (instance != null)
                    {
                        var properties = instance["properties"];
                        if (properties != null)
                        {
                            var dllNameProp = instance["properties"]["dll_name"];
                            if (dllNameProp != null)
                            {
                                var defaultVal = instance["properties"]["dll_name"]["default"];
                                if (defaultVal.ToString().Contains(dllName))
                                {
                                    instances.Add(file, instance);
                                }
                            }

                        }
                    }

                }

            }

            Console.WriteLine($"Found {instances.Count} schemas for '{dllName}'..");
            return instances;

        }
        private static Dictionary<string, JToken> GetAllTasksForDllName(string dllNameToSearch)
        {
            var instances = new Dictionary<string, JToken>();

            var files = GetAllFilesUnderLocation(_processBundlesLibraryLocation);
            foreach (var file in files)
            {
                if (file == null || file.ToLower().EndsWith("processinstance.json")) continue;
                var instanceJson = File.ReadAllText(file);
                var instance = JsonConvert.DeserializeObject<JToken>(instanceJson);
                var dllName = instance?["dll_name"];
                if (dllName == null) continue;
                if (dllName.ToString().Contains(dllNameToSearch))
                {
                    instances.Add(file, instance);
                }
            }
            Console.WriteLine($"Found {instances.Count} tasks for '{dllNameToSearch}'..");
            return instances;
            }
        }
    }