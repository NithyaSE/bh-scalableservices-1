﻿using System.IO;
using System.Net;
using System.Net.Http;
using BH.Workflow.Task.MessageRelay.UnitTests.Fakes;
using BH.Workflow.TestHarness;
using BHX.Workflow.Shared.Models.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BH.Workflow.Task.MessageRelay.UnitTests
{
    [TestClass]
    public class VisualizationTaskUnitTests
    {
        [TestMethod]

        public void Check_Viz_Task_With_Actual_Api()
        {
            var mockJsonInstance = File.ReadAllText("MockTaskInstances\\messagerelaytaskinstance1.json");
           
            var task = new MessageRelayTask()
            {
                BloodhoundTaskInstance = JsonConvert.DeserializeObject<BloodhoundTaskInstance>(mockJsonInstance)
            };

            var taskTest = new WorkflowTaskTester(task);
            taskTest.RunTask();

            var outputs = taskTest.Outputs;
            Assert.IsTrue(outputs.Count == 0);
        }


        [TestMethod]

        public void Check_Viz_Task_With_Mock_Api()
        {
            var mockJsonInstance = File.ReadAllText("MockTaskInstances\\messagerelaytaskinstance1.json");

            var mockApiCall = new FakeApiCall
            {
                Response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.Accepted,
                    Content = new StringContent("This is the result from a test")
                }
            };

            var task = new MessageRelayTask(mockApiCall)
            {
                BloodhoundTaskInstance = JsonConvert.DeserializeObject<BloodhoundTaskInstance>(mockJsonInstance)
            };

            var taskTest = new WorkflowTaskTester(task);
            taskTest.RunTask();

            var outputs = taskTest.Outputs;
            Assert.IsTrue(outputs.Count == 0);
        }

        [TestMethod]

        public void Check_Viz_Task_With_Mock_Api_Bad_Request()
        {

            var mockJsonInstance = File.ReadAllText("MockTaskInstances\\messagerelaytaskinstance1.json");

            var mockApiCall = new FakeApiCall
            {
                Response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Content = new StringContent("This is the result from a test")
                }
            };

            var task = new MessageRelayTask(mockApiCall)
            {
                BloodhoundTaskInstance = JsonConvert.DeserializeObject<BloodhoundTaskInstance>(mockJsonInstance)
            };

            var taskTest = new WorkflowTaskTester(task);
            taskTest.RunTask();

            var outputs = taskTest.Outputs;
            Assert.IsTrue(outputs.Count == 0);
        }

    }


}
