﻿using System.IO;
using BH.Shared.Email;
using BH.Workflow.Task.Email;
using BH.Workflow.TestHarness;
using BHX.Workflow.Shared.Models.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BH.Workflow.Task.UnitTests
{
    [TestClass]
    public class EmailTaskTests
    {
        [TestMethod]
        
        public void RunTask_GivenAValidInstance_CorrectlyGetsVariables()
        {
            var mockJsonInstance = File.ReadAllText("MockTaskInstances\\mocktaskinstance1.json");

            var emailOut = new FakeEmailOut();

            var task = new EmailTask(emailOut)
            {
                BloodhoundTaskInstance = JsonConvert.DeserializeObject<BloodhoundTaskInstance>(mockJsonInstance)
            };

            var taskTest = new WorkflowTaskTester(task);
            taskTest.RunTask();

            var isBodyHTML = emailOut.IsBodyHTML;
            var body = emailOut.Body;
            var subject = emailOut.Subject;
            var recpients = emailOut.Recepients;

            Assert.IsTrue(subject.Contains("This is a test Subject") && body.Contains("This is a test Body") && recpients.Contains("test@gmail.com"));
        }


        
    }




}
