﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Email.v1_0
{
    public class EmailMessage
    {
        public EmailMessage()
        {
            Attachments = new List<string>();
        }

        [JsonProperty("to")]
        public string To { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("isBodyHTML")]
        public bool IsBodyHTML { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("attachments")]
        public List<string> Attachments { get; set; }
    }
}
