﻿using System;
using System.Linq;
using Shared.Email;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Email.v1_0
{
    public class EmailTask : WorkflowTask
    {
        private IEmailOut EmailOut { get; set; }

        public EmailTask()
        {
            EmailOut = new EmailOut();
        }

        public EmailTask(IEmailOut emailOut)
        {
            EmailOut = emailOut;
        }
        public override IRunTaskReturn RunTask(JObject workingSet)
        {
            var ret = new FinishRunTaskReturn()
            {
                WorkingSet = workingSet
            };
            var emailWorkingSet = workingSet.ToObject<EmailInputPv>();
            if (emailWorkingSet?.Message == null) return ret;
            var recipients = emailWorkingSet?.Message.To.Split(';').ToList();
            Console.WriteLine("Running Email Task...");
            EmailOut.SendEmail(recipients, emailWorkingSet.Message.Subject, emailWorkingSet.Message.IsBodyHtml, emailWorkingSet.Message.Body, emailWorkingSet.Message.Attachments);
            return ret;

        }
    }
}
