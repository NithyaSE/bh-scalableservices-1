﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Migration
{
    class Program
    {
        private static string _schemaLocation => ConfigurationManager.AppSettings["SchemaLocation"];
        private static string _processBundlesLibraryLocation => ConfigurationManager.AppSettings["ProcessBundlesLibraryLocation"];
        private static string _oldDllName => "BH.Workflow.Task.DocumentProduction.v2_2.dll";
        private static string _newDllName => "BH.ScalableServices.Workers.Plugins.Workflow.Tasks.DocumentProduction.v2_2.dll";
        static void Main(string[] args)
        {
            UpdateTasks();
            UpdateSchemas();
            Console.ReadLine();
        }

        private static void UpdateTasks()
        {
            //find all files that have the right extension for 
            var subProcInstances = GetAllTasksForDllName(_oldDllName);
            //foreach file
            foreach (var item in subProcInstances)
            {
                var file = item.Key;
                Console.WriteLine($"Updating task {file}");
                var jTokTask = item.Value;
                var processInstanceFilePath = Path.Combine(Path.GetDirectoryName(file), "processinstance.json");
                var processinstanceJson = File.ReadAllText(processInstanceFilePath);
                var processinstance = JsonConvert.DeserializeObject<JToken>(processinstanceJson);
                jTokTask["dll_name"] = _newDllName;
                if (jTokTask["working_set_input_mappings_to_process_variables"] == null)
                {
                    //update timeout
                    UpdatePropertyName(jTokTask, "inputs", "working_set_input_mappings_to_process_variables");
                    var inputsOld = jTokTask["working_set_input_mappings_to_process_variables"];
                    if (inputsOld != null)
                    {
                        var plainTextTagMappings = inputsOld["plain_text_tag_mappings"];
                        JToken mappings = null;
                        if (plainTextTagMappings["is_mapped"] != null && plainTextTagMappings["is_mapped"].Value<bool>() && plainTextTagMappings["process_variable"] != null) // if value mapped
                        {
                            if (processinstance?["process_variables"]?[plainTextTagMappings["process_variable"].ToString()] != null)
                                mappings = JToken.Parse(processinstance["process_variables"][plainTextTagMappings["process_variable"].ToString()].ToString());
                        }
                        else
                        {
                            mappings = plainTextTagMappings["value"];
                        }
                        if (mappings != null && mappings is JArray)
                        {
                            foreach (var mapping in (JArray)mappings)
                            {
                                UpdateValueToBhMappable(mapping);
                            }
                        }
                    }
                    // Update outputs
                    UpdatePropertyName(jTokTask, "outputs", "working_set_output_mappings_to_process_variables");
                }
                File.Delete(file);
                File.WriteAllText(file, JsonConvert.SerializeObject(jTokTask));
            }
        }
        private static void UpdateValueToBhMappable(JToken element)
        {
            element["value"] = new JObject { { "is_mapped", element["is_mapped"] } ,
                { "process_variable", element["process_variable"] },
                { "value" , element["value"]}

            };
            ((JObject)element).Remove("is_mapped");
            ((JObject)element).Remove("process_variable");
        }

        private static void UpdateSchemaPropertyToBhMappable(JToken element)
        {
            element["value"] =
                JObject.Parse("{\r\n  \"title\": \"Tag Value\",\r\n                    \"type\": \"object\",\r\n                    \"properties\": {\r\n                      \"is_mapped\": {\r\n                        \"title\": \"Is Mapped\",\r\n                        \"type\": \"boolean\",\r\n                        \"default\": false\r\n                      },\r\n                      \"process_variable\": {\r\n                        \"title\": \"Process Variable\",\r\n                        \"type\": \"string\"\r\n                      },\r\n                      \"value\": {\r\n                        \"title\": \"Value\",\r\n                        \"type\": \"string\"\r\n                      }\r\n                    }\r\n                  }");
            ((JObject)element).Remove("is_mapped");
            ((JObject)element).Remove("process_variable");
        }
        private static void UpdatePropertyName(JToken parent, string oldPropertyName, string newPropName)
        {
            var oldProperty = parent[oldPropertyName];
            parent[newPropName] = oldProperty;
            ((JObject)parent).Remove(oldPropertyName);
        }
        private static void UpdateSchemas()
        {
            // Update Schemas
            var schemas = GetAllSchemasForDllName(_oldDllName);
            if (schemas == null) return;
            foreach (var inst in schemas)
            {
                var instance = inst.Value;
                var filePath = inst.Key;
                instance["properties"]["dll_name"]["default"] = _newDllName;
                if (instance["dll_name"] != null)
                {
                    instance["dll_name"] = _newDllName;
                }
                if (instance["properties"]?["working_set_input_mappings_to_process_variables"] == null)
                {
                    UpdatePropertyName(instance["properties"], "inputs", "working_set_input_mappings_to_process_variables");
                    instance["properties"]["working_set_input_mappings_to_process_variables"]["description"] = "Working Set Input mappings to Process Variables";
                    instance["properties"]["working_set_input_mappings_to_process_variables"]["title"] =
                        "Working Set Input Mappings";

                    UpdateSchemaPropertyToBhMappable(instance["properties"]?["working_set_input_mappings_to_process_variables"]?["properties"]?["plain_text_tag_mappings"]?["properties"]?["value"]?["items"]?["properties"]);
                    var mappingPropDefault = instance["properties"]?["working_set_input_mappings_to_process_variables"]?["properties"]?["plain_text_tag_mappings"]?["properties"]?["value"]?["default"];
                    if (mappingPropDefault != null && mappingPropDefault is JArray)
                    {
                        foreach (var mappingDefault in (JArray)mappingPropDefault)
                        {
                            if (mappingDefault != null)
                            {
                                UpdateValueToBhMappable(mappingDefault);
                            }
                        }
                    }

                    UpdatePropertyName(instance["properties"], "outputs", "working_set_output_mappings_to_process_variables");
                    instance["properties"]["working_set_output_mappings_to_process_variables"]["description"] = "Working Set Output Mappings";
                    instance["properties"]["working_set_output_mappings_to_process_variables"]["title"] =
                        "Working Set Output mappings to Process Variables";
                }
                Console.WriteLine($"Updating Schema {filePath}");
                File.Delete(filePath);
                File.WriteAllText(filePath, instance.ToString());
            }
        }
        private static List<string> GetAllFilesUnderLocation(string location)
        {
            var files = new List<string>();
            files.AddRange(Directory.GetFiles(location));
            var dirs = Directory.GetDirectories(location);
            foreach (var processDir in dirs)
            {
                files.AddRange(Directory.GetFiles(processDir, "*", SearchOption.AllDirectories));

            }
            return files;
        }
        private static Dictionary<string, JToken> GetAllSchemasForDllName(string dllName)
        {
            var instances = new Dictionary<string, JToken>();

            var files = GetAllFilesUnderLocation(_schemaLocation);
            foreach (var file in files)
            {
                if (file != null && file.ToLower().EndsWith("_schema.json"))
                {
                    var instanceJson = File.ReadAllText(file);
                    var instance = JsonConvert.DeserializeObject<JToken>(instanceJson);
                    if (instance != null)
                    {
                        var properties = instance["properties"];
                        if (properties != null)
                        {
                            var dllNameProp = instance["properties"]["dll_name"];
                            if (dllNameProp != null)
                            {
                                var defaultVal = instance["properties"]["dll_name"]["default"];
                                if (defaultVal.ToString().Contains(dllName))
                                {
                                    instances.Add(file, instance);
                                }
                            }

                        }
                    }

                }

            }

            Console.WriteLine($"Found {instances.Count} schemas for '{dllName}'..");
            return instances;

        }
        private static Dictionary<string, JToken> GetAllTasksForDllName(string dllNameToSearch)
        {
            var instances = new Dictionary<string, JToken>();

            var files = GetAllFilesUnderLocation(_processBundlesLibraryLocation);
            foreach (var file in files)
            {
                if (file == null || file.ToLower().EndsWith("processinstance.json")) continue;
                var instanceJson = File.ReadAllText(file);
                var instance = JsonConvert.DeserializeObject<JToken>(instanceJson);
                var dllName = instance?["dll_name"];
                if (dllName == null) continue;
                if (dllName.ToString().Contains(dllNameToSearch))
                {
                    instances.Add(file, instance);
                }
            }
            Console.WriteLine($"Found {instances.Count} tasks for '{dllNameToSearch}'..");
            return instances;
        }
    }
}