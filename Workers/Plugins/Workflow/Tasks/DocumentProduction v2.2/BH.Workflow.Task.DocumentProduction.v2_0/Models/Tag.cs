﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.DocumentProduction.v2_2.Models
{
    public class Tag
    {
        [JsonProperty("tag")]
        public string tag { get; set; }

        [JsonProperty("format")]
        public string Format { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}