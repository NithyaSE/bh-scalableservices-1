﻿namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.DocumentProduction.v2_2.Models
{
    public class Repository
    {
        public string repository { get; set; }

        public string path { get; set; }

        public string file { get; set; }

    }
}
