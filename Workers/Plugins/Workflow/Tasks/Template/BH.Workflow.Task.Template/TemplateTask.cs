﻿using System;
using System.Collections.Generic;
using BH.BusinessLogic.SDK;
using BH.BusinessLogic.Shared.BusinessModels;
using BH.BusinessLogic.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Shared.Helpers;
using Newtonsoft.Json;
using BH.Workflow.Task.Template.Models;
using Newtonsoft.Json.Linq;
namespace BH.Workflow.Task.Template
{
    /// <summary>
    /// Tasks should inherit from BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models.WorkflowTask in order to get basic "toolbox" features.
    /// 
    /// Steps for writing a Task:
    /// 1. After creating the solution, run the Init.exe.
    /// 2. All the required packages are included under the packages folder, and are added through a private nuget feed.
    /// 3. Inherit from WorkflowTask
    /// 4. Create Schema for Task - you can use the editor found here: http://jeremydorn.com/json-editor/  
    /// 5. Place schema in the Schemas folder through Visual Studio
    /// 6. Create Models for the Working set.
    /// 7. Implement RunTask(JObject workingSet) method and add all task functionality to UserRunTask method
    /// 8. Unless additional properties are to be added to working set, no changes are needed in the RunTask method.
    /// 9. Build project - dll and schema file will be found in the bin folder after Build
    /// 10. Upload Task dll, Workflow Schema, and any Files through the Workflow Designer: http://bhx-workflow-service.se-webapps.com
    /// 
    /// Note: that steps 1-8 have already been done for this Example Task.
    /// </summary>
    public class TemplateTask : WorkflowTask
    {
        public IBusinessLogicClient BusinessLogicClient { get; set; }
        public TemplateTask()
        {
            BusinessLogicClient = new BusinessLogicClient();
        }
        public TemplateTask(IBusinessLogicClient businessLogic)
        {
            BusinessLogicClient = businessLogic;
        }
        /// <summary>
        /// RunTask() is the entry point for every Task.
        /// This method provides boilerplate code that prepares the working set.
        /// No changes needed in this method
        /// </summary>
        private IRunTaskReturn UserRunTask(TaskWorkingSet workingSet)
        {
            var processRunId = SystemTaskVariables.ProcessRunId;
            //Using Task Inputs. The input task variables are auto initialized for the task
            var exampleTaskVar1 = workingSet.ExampleTaskVar1;
            if (exampleTaskVar1 != null)
                LogEntryWriter.LogMessage($"Contents of ExampleTaskVar1: {exampleTaskVar1}", processRunId);
            var exampleTaskVar2 = workingSet.ExampleTaskVar2;
            if (exampleTaskVar2 != null)
                LogEntryWriter.LogMessage($"Contents of ExampleTaskVar2: {JsonConvert.SerializeObject(exampleTaskVar2.ExamplePropList)}", processRunId);
            //can get data using the Business logic SDK's Get method
            BusinessLogicClient.Setup(Settings.BloodhoundDataStoreEndpoint, Settings.BloodhoundDataStoreUsername, Settings.BloodhoundDataStorePassword);
            var businessLogicGetRequestBodyItem = new GetPoint();
            businessLogicGetRequestBodyItem.FullNames.Add("Point Fullname");
            businessLogicGetRequestBodyItem.Tags.Add(new BloodhoundTag()
            {
                Category = "Tag Category",
                Name = "Tag Name"
            });
            var data = BusinessLogicClient.Get("My Customer", new List<GetPoint> { businessLogicGetRequestBodyItem });
            // can POST data using the Business Logic SDK's Post method
            BusinessLogicClient.Post(data);
            // Any of the properties from the working set can be used as part of the task output and be mapped to a process variable.
            // Just set the property on the working set and return it as part of the FinishRunTaskReturn.
            workingSet.Result = "This can be used in another process later.";
            return new FinishRunTaskReturn();
        }
        /// <summary>
        /// RunTask() is the entry point for every Task.
        /// This method provides boilerplate code that prepares the working set.
        /// </summary>
        public override IRunTaskReturn RunTask(JObject workingSet)
        {
            var templateWorkingSet = workingSet.ToObject<TaskWorkingSet>();
            var runTaskReturn = UserRunTask(templateWorkingSet);
            // New properties can also be added to the working set and be made available for mapping.
            var returnWorkingSet = JObject.FromObject(templateWorkingSet);
            //returnWorkingSet["output_property1"] = "Output from task added to the working set";
            runTaskReturn.WorkingSet = returnWorkingSet;
            return runTaskReturn;
        }
    }
}
