﻿using Newtonsoft.Json;

namespace BH.Workflow.Task.Template.Models
{
    /// <summary>
    /// Edit this class to add working set properties.
    /// </summary>
    public class TaskWorkingSet
    {
        [JsonProperty("example_task_var_1")]
        public string ExampleTaskVar1 { get; set; }

        [JsonProperty("example_task_var_2")]
        public ExampleComplexType ExampleTaskVar2 { get; set; }
        [JsonProperty("result")]
        public string Result { get; set; }
    }
}
