﻿using System.Net.Http;
using BHX.Workflow.Shared.Models.Interfaces;

namespace BH.Workflow.Task.Visualization.UnitTests.Fakes
{
    public class FakeApiCall : IApiRequest
    {
        public object Request { get; set; }
        public HttpResponseMessage Response { get; set; }

        public HttpResponseMessage HandleRequest()
        {
            return Response;
        }
    }
}
