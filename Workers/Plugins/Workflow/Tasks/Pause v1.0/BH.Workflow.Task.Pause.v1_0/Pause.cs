﻿using System;
using System.Configuration;
using System.Net.Http;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Shared.Helpers;
using Shared.WebClient;
using Shared.WebClient.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Pause.v1_0
{
    public class Pause : WorkflowTask
    {
        public override IRunTaskReturn RunTask(JObject workingSet)
        {
            Console.WriteLine("Running Pause task! The name is : " + SystemTaskVariables.WorkflowTaskInstanceName);
            if (!SystemTaskVariables.IsRehydrated)
            {
                var customerName = workingSet["customer_name"]?.ToString();
                if (string.IsNullOrWhiteSpace(customerName))
                {
                    throw new Exception("Please provide a value for the customer name.");
                }
                //first run
                var resumeToken = Guid.NewGuid().ToString("N");
                var relayDlls = JArray.Parse(workingSet["message_relay_dll_names"].ToString());
                var bhMessage = JToken.Parse(workingSet["bh_message"].ToString());
                SendMessage(SystemTaskVariables.ProcessRunId, resumeToken, customerName, bhMessage, relayDlls,workingSet);
                return new PauseRunTaskReturn()
                {
                    ResumeToken = resumeToken,
                    WorkingSet = workingSet
                };
            }
            //resumed task
            LogEntryWriter.LogMessage($"Rehydrated the Task", SystemTaskVariables.ProcessRunId);
            return new FinishRunTaskReturn()
            {
                WorkingSet = workingSet
            };
        }
        public void SendMessage(string runId,string resumeToken,string customerName,JToken bhMessage,JArray relayDlls,JObject workingSet)
        {
            var baseUri = Settings.MessageRelayBaseUrl;
            var uri = Settings.MessageRelayUri;
            var creds = Settings.MessageRelayApiCreds;
            // add customer name to the bhMessage
            bhMessage["customer_name"] = customerName;
           bhMessage["description"] =$"{bhMessage["description"]} <br>Resume url - {ConfigurationManager.AppSettings["ScalableApiEndPoint"]}start?run_id={runId}&resume_token={resumeToken}<br>" +
                $"Working Set - {JsonConvert.SerializeObject(workingSet)}";
            var body = new {            
                bh_messages = new JToken[] { bhMessage },
                message_relay_dll_names = relayDlls
            };
            var request = new HttpRequest(HttpVerb.Post) { BaseUri = baseUri };
            request.PathFragments.Add(new HttpPathFragment("uri", uri)); 
            if (creds != null)
                request.Headers.Add(new HttpHeader("Authorization", creds));

            request.Body = new StringContent(JsonConvert.SerializeObject(body));

            var response = HandleWfsRequest(request);

            var contentAsString = response.Content.ReadAsStringAsync().Result;

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Unable to communicate with the message relay service. " + response.ReasonPhrase + " " + contentAsString);
            }
            LogEntryWriter.LogMessage($"Response from message relay API {contentAsString}", runId);
        }
        public HttpResponseMessage HandleWfsRequest(HttpRequest request)
        {
            var response = HttpHelper.HandleRequest(request);
            return response;
        }
    }
}
