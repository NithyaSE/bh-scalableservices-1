﻿using System;
using System.Collections.Generic;
using BH.ScalableServices.Brokers.SDKBase;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Scheduler.v2_0.Models;
using BH.ScalableServices.Workers.Shared.Helpers;
using BH.Server.Scheduler.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Schedule.v2_0
{
    public class Schedule : WorkflowTask
    {
        public override IRunTaskReturn RunTask(JObject workingSet)
        {
            Console.WriteLine("Running Schedule Task! The name is: " + SystemTaskVariables.WorkflowTaskInstanceName);

            var schedulerWorkingSet = workingSet.ToObject<SchedulerWorkingSet>();

            //Base TS
            var TSString = schedulerWorkingSet.BaseTS;
            var baseTS = DateTime.Parse(TSString);

            var client = new Server.Scheduler.SDK.Core.SchedulerClient(Settings.WorkflowSchedulerBaseUri, Settings.WorkflowSchedulerCreds);

            //Doesn't work.  Defaults to 30 days for month (P1M), so it can't run something on the 2nd of each month.
            //var interval = GenTools.TaskInstance.SafeStringToTimeSpan(GetTaskVariable("Interval"));
            
            var processToSchedule = schedulerWorkingSet.ProcessToSchedule;
            var TargetURL = Settings.WorkflowSchedulerTargetUrl;
            var wfsCreds = ((ScalableServiceWebClient)StatusUpdater.ServiceWebClient).Header;  //WFS task, so scalable service will be WFS task
            var targetHeaders = new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>("Authorization", wfsCreds.Value) };

            var schedule = new ScheduleInputs
            {
                BaseTime = baseTS,
                Uri = TargetURL,
                Parameters = new List<KeyValuePair<string, string>> { new KeyValuePair<string, string> ("process_instance_name", processToSchedule ) },
                Body = "",
                Headers = targetHeaders,
                Interval = schedulerWorkingSet.Interval

            };
            
            try
            {
                var instance = client.ScheduleHTTPRequest(schedule);
                LogEntryWriter.LogMessage($"\nScheduler successfully accepted schedule. \n Instance Details: {JsonConvert.SerializeObject(instance)}", SystemTaskVariables.ProcessRunId);
            }
            catch (Exception ex)
            {
                LogEntryWriter.LogMessage($"\nSchedler failed to accept schedule. \n Input Details: {JsonConvert.SerializeObject(schedule)}", SystemTaskVariables.ProcessRunId);
                throw;
            }

            return new FinishRunTaskReturn();

        }
      }
}
