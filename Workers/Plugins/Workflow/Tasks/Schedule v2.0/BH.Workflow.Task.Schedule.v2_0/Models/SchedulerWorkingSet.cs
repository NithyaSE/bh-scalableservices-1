﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Scheduler.v2_0.Models
{
   public class SchedulerWorkingSet
    {
        [JsonProperty("interval")]
        public string Interval { get; set; }
        [JsonProperty("base_ts")]
        public string BaseTS { get; set; }
        [JsonProperty("process_to_schedule")]
        public string ProcessToSchedule { get; set; }
    }
}
