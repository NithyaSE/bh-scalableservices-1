﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.RunSubProcessOneIteration.v2_0.Models
{
    public class SubProcessRegistryItem
    {
        [JsonProperty("run_id")]
        public string RunId { get; set; }
        [JsonProperty("iteration")]
        public string Iteration { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("is_finished")]
        public bool IsFinished { get; set; } 
    }
}
