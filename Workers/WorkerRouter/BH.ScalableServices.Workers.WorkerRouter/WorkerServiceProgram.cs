﻿using Shared.PluginRouter;
using BH.ScalableServices.Brokers.SDKBase;

namespace BH.ScalableServices.Worker.WorkerRouter
{
    public class WorkflowWorkerService
    {
        public static void Main(string[] args)
        {
            IScalableServiceWebClient webClient = new ScalableServiceWebClient();
            IPluginRouter pluginRouter = new PluginRouter();
            WorkerServiceConfig<Worker>.Configure("Worker Router",
                "Worker Router", "BH_Worker_Router", webClient, pluginRouter);
        }
    }
}
