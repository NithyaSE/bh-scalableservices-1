﻿using System;
using System.Threading;
using Shared.PluginRouter;
using BH.ScalableServices.Brokers.SDKBase;
using Shared.SerilogsLogging;
using Serilog;

namespace BH.ScalableServices.Worker.WorkerRouter
{
    public class WorkerServiceRunner<T> where T : Worker, new()
    {
        private Thread _runThread;
        private T _workerServiceWorker;
        public string ServiceName { get; set; }

        private IScalableServiceWebClient _serviceWebClient;
        private IPluginRouter _pluginRouter;
        public WorkerServiceRunner(IScalableServiceWebClient serviceWebClient,IPluginRouter pluginRouter=null)
        {
            _serviceWebClient = serviceWebClient;
            _pluginRouter = pluginRouter;
        }
        public void Start()
        {
            _workerServiceWorker = new T()
            {
                ServiceWebClient = _serviceWebClient,
                PluginRouter = _pluginRouter,
                ServiceName = ServiceName
            };
            _runThread = new Thread(_workerServiceWorker.Go);
            _runThread.Start();
            Console.WriteLine($@"{ServiceName} Started");
           
            Log.Logger = new LoggerConfiguration()
                        .ReadFrom.AppSettings()
                        .CreateLogger();

            Log.Logger.AddMethodName().Information("Summary='{ServiceName} Started'",ServiceName);
        }

        public void Stop()
        {
            _workerServiceWorker.RequestStop();
            Console.WriteLine("{0} Stopped", ServiceName);
            Log.Logger.AddMethodName().Information("Summary='{ServiceName} Stopped'", ServiceName);
        }


    }
}
