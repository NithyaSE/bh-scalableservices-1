﻿using System;
using System.Linq;
using System.Reflection;
using BH.ScalableServices.Workers.Plugins.Workflow.Interfaces;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{
    public class WorkflowPluginLoader<T> : IPluginLoader<T>
    {
        public WorkflowPluginLoader()
        {
        }

        public T LoadPlugin<T>(string assemblyFile)
        {
            var plugin = Assembly.LoadFrom(assemblyFile).GetTypes()
                            .Where(t => InheritsFrom(t, typeof(T)))
                            .Select(t => (T)Activator.CreateInstance(t)).FirstOrDefault();

            return plugin;
        }


        private bool InheritsFrom(Type t, Type baseType)
        {
            var cur = t.BaseType;

            while (cur != null)
            {
                if (cur == baseType)
                {
                    return true;
                }

                cur = cur.BaseType;
            }

            return false;
        }
    }
}
