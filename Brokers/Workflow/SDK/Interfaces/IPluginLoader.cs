﻿namespace BH.ScalableServices.Workers.Plugins.Workflow.Interfaces
{

    public interface IPluginLoader<T>
    {
        T LoadPlugin<T>(string assemblyFile);
    }
}
