﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using BH.ScalableServices.Workers.CustomExceptions;
using BH.ScalableServices.Workers.Helpers;
using BH.ScalableServices.Workers.Plugins.Workflow.Interfaces;
using BH.Shared.PluginRouter;
using BH.Shared.ProcessHelper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;
using Shared.SerilogsLogging;
using Shared.WebClient.Models;
using TinyIoC;

namespace BH.ScalableServices.Workers.Plugins.Workflow
{
    public class WorkflowRunner
    {
        public IWorkflowProcessInstance WorkflowProcessInstance { get; set; }
        public  IWorkflowTaskInstance CurrentWorkflowTaskInstance { get; set; }
        public static string TasksLocation { get; set; }
        public IPluginRouter PluginRouter { get; set; }
        public IWorkflowServiceClient WfsClient { get; set; }
        public IProcessRunsRepository ProcessRunsRepository { get; set; }
        private WorkflowSettings _settings { get; set; }
        public WorkflowRunner(IWorkflowServiceClient wfsClient, WorkflowSettings settings,IProcessRunsRepository processRunsRepo, IPluginRouter pluginLoader)
        {
            WfsClient = wfsClient;
            _settings = settings;
            ProcessRunsRepository = processRunsRepo;
            PluginRouter = pluginLoader;
        }
        public void RunProcess()
        {
            Log.Logger.AddMethodName().Information("Summary='Running process {ProcName}, RunId {RunId}'", WorkflowProcessInstance?.Name, WorkflowProcessInstance?.RunId);
            var processPath =  Path.Combine(_settings.ProcessRunsLocation, WorkflowProcessInstance.RunId);
            Log.Logger.AddMethodName().Debug("Summary='Process path {ProcPath}'", processPath);
            WorkflowProcessInstance.ProcessVariables["$RunId"] = WorkflowProcessInstance.RunId;
            if (WorkflowProcessInstance.ProcessVariables.All(x => x.Key != "$current_task_execution_order_index"))
            {
                WorkflowProcessInstance.ProcessVariables.Add("$current_task_execution_order_index", 0);
                WorkflowProcessInstance.ProcessVariables.Add("$current_error_path_task_execution_order_index_index", 0);
            }
            var currentPathIndexString = WorkflowProcessInstance.ProcessVariables.FirstOrDefault(x => x.Key == "$current_task_execution_order_index").Value.ToString();
            var currentPathErrorIndexString = WorkflowProcessInstance.ProcessVariables.FirstOrDefault(x => x.Key == "$current_error_path_task_execution_order_index_index").Value.ToString();
            //temp logging... or maybe permanent
            Log.Logger.AddMethodName().Information("Summary='Current task execution order index {Val} and error execution order index {ErrVal}'", currentPathIndexString, currentPathErrorIndexString);
            var index = int.Parse(currentPathIndexString);
            var errorIndex = int.Parse(currentPathErrorIndexString);
            //Scan Computed PVs
            string timezone = null;
            if (!WorkflowProcessInstance.ProcessVariables.ContainsKey("!ReferenceTimeZone") || string.IsNullOrEmpty(timezone =
                    WorkflowProcessInstance.ProcessVariables["!ReferenceTimeZone"]?.ToString()))
            {
                LogEntryWriter.LogMessage("No reference timezone provided. Defaulting to '(UTC) Coordinated Universal Time'", WorkflowProcessInstance.RunId);
                timezone = "(UTC) Coordinated Universal Time";
            }
            WorkflowProcessInstance.ComputeProcessVariableKeywords(timezone);
            LogProcessVariables();
            if (errorIndex !=0)
            {
                RunWarningPath(errorIndex);
                return;
            }
            try
            {
               RunNormalPath(index);
               WfsClient.PostProcessOutputs(WorkflowProcessInstance);
               LogEntryWriter.LogObject(WorkflowProcessInstance.ProcessVariables, "The Process Variables running the Output Pv mappings", WorkflowProcessInstance.RunId);
            }
            catch (ExitApplicationCustomException)
            {
                throw;
            }
            catch (Exception ex)
            {
                Log.Logger.AddMethodName().Error("Summary='Error in Workflow Process {@ProcName},Running Error Path' Details='{@Ex}'", WorkflowProcessInstance.Name, ex);
                LogEntryWriter.LogMessage($"Error Occured - {ex.Message}. Running Warning Path.",WorkflowProcessInstance.RunId);
                RunWarningPath(0);
            }
            Log.Logger.AddMethodName().Information("Summary='Finished running process {ProcName}, RunId {RunId}'", WorkflowProcessInstance?.Name, WorkflowProcessInstance?.RunId);
         }

        private void RunNormalPath(int index)
        {
            RunTasks(WorkflowProcessInstance.TaskExecutionOrder, index, "$current_task_execution_order_index");
        }
        private void RunWarningPath(int errorIndex)
        {
            try
            { 
                RunTasks(WorkflowProcessInstance.OnErrorExecutionOrder, errorIndex,
                "$current_error_path_task_execution_order_index_index");
            }
            catch (ExitApplicationCustomException)
            {
                throw;
            }
            catch (Exception exp)
            {
                Console.WriteLine($@"{ DateTime.Now:s}-{WorkflowProcessInstance.Name}- Failed to run the warning path with exception - {exp.Message}");
                throw new MessageProcessingFailedCustomException($"Failed to run the warning path with exception - {exp.Message}", exp);
            }
        }       
        private void PauseProcess(PauseRunTaskReturn finishRunTaskReturn,string currentTaskFileName)
        {
            Log.Logger.AddMethodName().Information("Summary='Pausing Workflow Process {@ProcName}'", WorkflowProcessInstance.Name);
            WorkflowProcessInstance.ProcessVariables["$resume_token"] = finishRunTaskReturn.ResumeToken;
            DehydrateWfsProcessAndTaskInstance(currentTaskFileName);
        }
        private void Dehydrate(DehydrateRunTaskReturn dehydrateRunTaskReturn,string currentTaskFileName)
        {
            DehydrateWfsProcessAndTaskInstance(currentTaskFileName);
            ScheduleProcessToRunAfterInterval(dehydrateRunTaskReturn.CheckInterval);
        }
        private void DehydrateWfsProcessAndTaskInstance(string currentTaskFileName)
        {
            ProcessRunsRepository.DehydrateTempTask(WorkflowProcessInstance.RunId, currentTaskFileName, CurrentWorkflowTaskInstance);
            LogEntryWriter.LogMessage($"Dehydrating task : {CurrentWorkflowTaskInstance.Name}", WorkflowProcessInstance.RunId);
            Log.Logger.AddMethodName().Information("Summary='Dehydrating Workflow Process {@ProcName}'", WorkflowProcessInstance.Name);
            StatusUpdater.SetIsDehydrated(WorkflowProcessInstance.RunId, "Workflow Service Worker", $"Dehydrating process - {WorkflowProcessInstance.Name}");
            ProcessRunsRepository.SaveProcessInstance(WorkflowProcessInstance);
        }
        private void ScheduleProcessToRunAfterInterval(string checkInterval)
        {
            var targetUrl = $"{_settings.WorkflowSchedulerTargetUrl.Replace("process_instance_name=", "")}run_id={WorkflowProcessInstance.RunId}";
            var wfsCreds = _settings.WorkflowServiceCreds;
            var targetHeaders = new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>("Authorization", wfsCreds) };
            var creds = _settings.WorkflowSchedulerCreds;
            var checkIntervalInSeconds = checkInterval;         
            var scheduleVariables = new  { BaseTime = DateTime.UtcNow, Headers = targetHeaders, Url = targetUrl, Interval = checkIntervalInSeconds };
            var request = new HttpRequest(HttpVerb.Post) { BaseUri = _settings.WorkflowSchedulerBaseUri };
            request.PathFragments.Add(new HttpPathFragment
            {
                Key = "SchedulerCommand",
                Uri = _settings.WorkflowSchedulerUri
            });
            request.Body = new StringContent(JsonConvert.SerializeObject(scheduleVariables));
            Log.Logger.AddMethodName().Information("Scheduling process run by calling - {@req} with {@body}", request.BaseUri, JsonConvert.SerializeObject(scheduleVariables));
            request.Headers.Add(new HttpHeader
            {
                HeaderName = "Authorization",
                Value = creds
            });
            var apiReq = TinyIoCContainer.Current.Resolve<IApiRequest>();
            apiReq.Request = request;
            var result = apiReq.HandleRequest();
            var content = result.Content.ReadAsStringAsync().Result;
            if (!result.IsSuccessStatusCode)
            {
                throw new Exception($"\nFailed with status code: {(int)result.StatusCode}<br> Reason Phrase: {result.ReasonPhrase}");
            }
            LogEntryWriter.LogMessage($"\nReceived response with status code : {(int)result.StatusCode}\n Message: {content}",WorkflowProcessInstance.RunId);
        }

        private void RunTasks(IList<string> taskExecutionOrder, int taskExecutionIndex, string indexPVName)
        {
            if (taskExecutionOrder == null) return;
            foreach (var nextTaskName in taskExecutionOrder.Skip(taskExecutionIndex))
            {
                var runTaskReturn = RunTask(nextTaskName);
                if (runTaskReturn is DehydrateRunTaskReturn || runTaskReturn is PauseRunTaskReturn)
                {
                    return;
                }
                WorkflowProcessInstance.ProcessVariables[indexPVName] = ++taskExecutionIndex;
            }
        }
        private IRunTaskReturn RunTask(string taskName)
        {
            string currentTaskFileName = null;
            InitializeCurrentWorkflowTaskInstance(taskName, out currentTaskFileName);
            try
            {
                if (CurrentWorkflowTaskInstance == null)
                    throw new Exception(
                        $"The task instance with name {taskName} could not be found in the Runs directory");
                if (!IsCurrentWfsTaskInstanceActive())
                {
                    LogEntryWriter.LogMessage(
                        $"Task instance {taskName} has property is_active set to false. Skipping the task instance.",
                        WorkflowProcessInstance.RunId);
                    return new FinishRunTaskReturn();
                }

                var runTaskReturn = RunCurrentTask();
                LogProcessVariables();
                if (runTaskReturn is DehydrateRunTaskReturn)
                {
                    var dehydrateRunTaskReturn = (DehydrateRunTaskReturn) runTaskReturn;
                    CurrentWorkflowTaskInstance.WorkingSet = dehydrateRunTaskReturn.WorkingSet;
                    Dehydrate(dehydrateRunTaskReturn, currentTaskFileName);
                }
                else if (runTaskReturn is PauseRunTaskReturn)
                {
                    var pauseRunTaskReturn = (PauseRunTaskReturn) runTaskReturn;
                    CurrentWorkflowTaskInstance.WorkingSet = pauseRunTaskReturn.WorkingSet;
                    PauseProcess(pauseRunTaskReturn, currentTaskFileName);
                }
                else if (runTaskReturn is FinishRunTaskReturn)
                {
                    try
                    {
                        var finishRunTaskReturn = (FinishRunTaskReturn) runTaskReturn;
                        LogEntryWriter.LogObject(finishRunTaskReturn.WorkingSet,
                            $"The working set for {CurrentWorkflowTaskInstance.Name}", WorkflowProcessInstance.RunId);
                        MapCurrentTaskWorkingSetToPvs(finishRunTaskReturn);
                        LogProcessVariables();
                    }
                    finally
                    {
                        ProcessRunsRepository.RemoveTempTaskFile(WorkflowProcessInstance.RunId, currentTaskFileName);
                        WorkflowProcessInstance.ProcessVariables.Remove("$current_task_file_name");
                    }
                }

                return runTaskReturn;
            }
            catch
            {
                ProcessRunsRepository.RemoveTempTaskFile(WorkflowProcessInstance.RunId, currentTaskFileName);
                WorkflowProcessInstance.ProcessVariables.Remove("$current_task_file_name");
                throw;
            }
           
        }
        private void InitializeCurrentWorkflowTaskInstance(string taskName,out string currentTaskFileName)
        {
            CurrentWorkflowTaskInstance = null;
            currentTaskFileName = "";
            bool isRehydrated = false;
            if (WorkflowProcessInstance.ProcessVariables.ContainsKey("$current_task_file_name"))
            {
                currentTaskFileName = WorkflowProcessInstance.ProcessVariables["$current_task_file_name"]?.ToString();
                CurrentWorkflowTaskInstance = (WorkflowTaskInstance)ProcessRunsRepository.RehydrateTempTask(WorkflowProcessInstance.RunId, currentTaskFileName);
                isRehydrated = true;
            }
            else
            {// new task first run
                CurrentWorkflowTaskInstance = (WorkflowTaskInstance)GetTaskInstance(taskName);
                currentTaskFileName = ProcessRunsRepository.CreateTempTaskInstance(CurrentWorkflowTaskInstance, WorkflowProcessInstance.RunId);
                WorkflowProcessInstance.ProcessVariables["$current_task_file_name"] = currentTaskFileName; // will be saved when des
                LogEntryWriter.LogObject(CurrentWorkflowTaskInstance.WorkingSetInputMappingsToPvs,$"The Working Set Input mappings to process variables for {CurrentWorkflowTaskInstance.Name}",WorkflowProcessInstance.RunId);
                LogEntryWriter.LogObject(CurrentWorkflowTaskInstance.WorkingSetOutputMappingsToPvs,$"The Working Set Output mappings to process variables for {CurrentWorkflowTaskInstance.Name}", WorkflowProcessInstance.RunId);
            }
            InitializeSystemTaskVariablesForCurrentWfsTask(isRehydrated);
        }

        private void InitializeSystemTaskVariablesForCurrentWfsTask(bool isRehydrated)
        {
            var systemTvs = new JObject
            {
                ["$run_id"] = WorkflowProcessInstance.RunId,
                ["$is_rehydrated"] = isRehydrated,
                ["$workflow_task_instance_name"] = CurrentWorkflowTaskInstance.Name
            };
            foreach (var requiredPv in WorkflowProcessInstance.ProcessVariables.Where(pv => pv.Key.StartsWith("!")))
            {
                systemTvs[requiredPv.Key] = JToken.FromObject(requiredPv.Value) ;
            }
            CurrentWorkflowTaskInstance.SystemTaskVariables = systemTvs;
        }

        private void MapCurrentTaskWorkingSetToPvs(FinishRunTaskReturn finishRunTaskReturn)
        {
            if (finishRunTaskReturn.WorkingSet == null) return;
            //Map the top level output tvs to pvs
            foreach (var mapping in CurrentWorkflowTaskInstance.WorkingSetOutputMappingsToPvs)
            {
                var outputPropertyName = mapping.Key;
                var processVariableName = mapping.Value;
                if (string.IsNullOrEmpty(processVariableName)) continue;
                if (!WorkflowProcessInstance.ProcessVariables.ContainsKey(processVariableName))
                    throw new Exception($"The process variable {processVariableName} mapped to working set property {outputPropertyName} does not exist in the list of process variables");
                if (finishRunTaskReturn.WorkingSet.ContainsKey(outputPropertyName))
                {
                    WorkflowProcessInstance.SetProcessVariable(processVariableName, finishRunTaskReturn.WorkingSet[outputPropertyName]);
                }
                else
                {
                    throw  new Exception($"The property {outputPropertyName} could not be found on the output task variables provided in the finish task return");
                }

            }
        }
       private IWorkflowTaskInstance GetTaskInstance(string taskName)
        {
            var taskInstance = ProcessRunsRepository.GetTaskInstanceFromRunId(WorkflowProcessInstance.RunId, taskName);
            if (taskInstance != null) return taskInstance;
            LogEntryWriter.LogMessage($"Task instance {taskName} could not be found. Skipping the task instance.",WorkflowProcessInstance.RunId);
            return null;
        }
        private bool IsCurrentWfsTaskInstanceActive()
        {
            return CurrentWorkflowTaskInstance.IsActive.IsMapped ? GetMappedVariable(CurrentWorkflowTaskInstance.IsActive.ProcessVariable, typeof(bool)) : CurrentWorkflowTaskInstance.IsActive.Value;
        }
       private void LogProcessVariables()
        {
            var jsonPVs = JsonConvert.SerializeObject(WorkflowProcessInstance.ProcessVariables);
            LogEntryWriter.LogMessage($"Process Variables: {jsonPVs}",WorkflowProcessInstance.RunId);
        }
        public IRunTaskReturn RunCurrentTask()
        {
            IRunTaskReturn runTaskReturn;
            LogEntryWriter.LogMessage(CurrentWorkflowTaskInstance.Name + " (Task) : Started",WorkflowProcessInstance.RunId);
            Console.WriteLine($@"{DateTime.Now:s}-{CurrentWorkflowTaskInstance.TaskDllName}-{CurrentWorkflowTaskInstance.Name} - Started");
            Log.Logger.AddMethodName().Information("Summary='{TaskInstanceName},{TaskDllName} Started'", CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName);
            try
            {   //Run Stinky code to kill all rogue excel instaces
                //TODO: This issue could use some investigation for the root cause of rogue excels and this code could be cleaned up.
                ProcessHelper.TryKillAllProcessInstancesByName("Excel");
                ProcessHelper.TryKillAllProcessInstancesByName("WinWord");
                var workflowTask = GetWorkflowTask();
                workflowTask.SetSystemTaskVariablesJObject(CurrentWorkflowTaskInstance.SystemTaskVariables);
                InitializeCurrentWfsTaskWorkingSet();
                LogEntryWriter.LogObject(CurrentWorkflowTaskInstance.WorkingSet, $"The working set task variables for {CurrentWorkflowTaskInstance.Name}",WorkflowProcessInstance.RunId);
                runTaskReturn = workflowTask.RunTask(CurrentWorkflowTaskInstance.WorkingSet);
            }
            catch (Exception ex)
            {
                Console.WriteLine($@"{DateTime.Now:s}-{CurrentWorkflowTaskInstance.TaskDllName}-{CurrentWorkflowTaskInstance.Name} - Failed");
                LogEntryWriter.LogMessage(ex.ToString(),WorkflowProcessInstance.RunId);
                Log.Logger.AddMethodName().Information("Summary='{TaskInstanceName},{TaskDllName} Failed' Details='{@Ex}'", CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName, ex);
                throw;
            }
            Console.WriteLine($@"{DateTime.Now:s}-{CurrentWorkflowTaskInstance.TaskDllName}-{CurrentWorkflowTaskInstance.Name} - Completed");
            LogEntryWriter.LogMessage(CurrentWorkflowTaskInstance.Name + " (Task) : Finished",WorkflowProcessInstance.RunId);
            Log.Logger.AddMethodName().Information("Summary='{@TaskInstanceName},{TaskDllName} Finished'", CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName);
            return runTaskReturn;
        }
        #region Logging Methods
        /// <summary>
        /// Writes to the log in the process instances folder.
        /// </summary>
        /// <param name="msg">Message to be logged</param>
        /// <param name="type"></param>
      
        #endregion
        public WorkflowTask GetWorkflowTask()
        {
            WorkflowTask task = null;
            Log.Logger.AddMethodName()
                .Debug("Summary='Attempting to load the plugin dlls for task {@taskName}. Task Dll Name - {@taskDllName}'",
                    CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName);
            //try to load dlls 
            var noOfAttemps = 3;
            var attemptNo = 0;
            var success = false;
            while (attemptNo++ < noOfAttemps && !success)
            {
                try
                {
                    Log.Logger.AddMethodName()
                        .Debug("Summary='Loading the plugin dlls for task {@taskName}. Task Dll Name - {@taskDllName}' in attempt {@noOfAttempt}",
                            CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName, attemptNo);
                    task = PluginRouter.GetPlugins<WorkflowTask>(CurrentWorkflowTaskInstance.TaskDllName,
                        new List<string>
                        {
                            AppDomain.CurrentDomain.BaseDirectory,
                            TasksLocation
                        }).FirstOrDefault();
                    if (task != null)
                        success = true;
                    else
                    {
                        // failure without com exception, do as before
                        break;
                    }
                }
                catch (Exception exp)
                {
                    Log.Logger.AddMethodName()
                        .Warning("Summary='Failed to load the plugin dlls for task {@taskName}. Task Dll Name - {@taskDllName}' in attempt {@noOfAttempt} with Exception {@exp}",
                            CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName, attemptNo, exp);

                }
            }
            // success
            if (success)
            {
                Log.Logger.AddMethodName().Debug("Summary='Plugin dlls loaded successfully for task {@taskName}. Task Dll Name - {@taskDllName}'", CurrentWorkflowTaskInstance.Name, CurrentWorkflowTaskInstance.TaskDllName);
                task.Settings = _settings;
            }
            else
            {
                if (attemptNo < 3)
                    throw new Exception("No Task was found with name : " + CurrentWorkflowTaskInstance.TaskDllName +
                                        " Make sure there is a Task dll with that name.");
                //Com exception 3 times. Requeue the message,log and kill yourself
                LogEntryWriter.LogMessage("Worker failed to load COM dlls three times. Requeueing the process and killing the worker.",WorkflowProcessInstance.RunId);
                Log.Logger.Fatal("Summary='!!!!!!!Failed to load task plugin with COM Exception thrice!!!!!!");
                Log.Logger.Fatal("Summary='!!!!!!!Dehydrating the process and putting it back on queue!!!!!!");
                StatusUpdater.SetIsDehydrated(WorkflowProcessInstance.RunId, "Workflow Service Worker", $"Dehydrating process - {WorkflowProcessInstance.Name}");
                ProcessRunsRepository.SaveProcessInstance(WorkflowProcessInstance);
                throw new ExitApplicationCustomException("Worker failed to load COM dlls three times.");
            }
            return task;
        }
        private void InitializeCurrentWfsTaskWorkingSet()
        {
            if (bool.Parse(CurrentWorkflowTaskInstance.SystemTaskVariables["$is_rehydrated"].ToString())) return;
            if (CurrentWorkflowTaskInstance.WorkingSetInputMappingsToPvs == null) return ;
            var workingSet = CurrentWorkflowTaskInstance.WorkingSetInputMappingsToPvs.DeepClone();
            InitializeWorkingSet(workingSet);
            CurrentWorkflowTaskInstance.WorkingSet = workingSet.ToObject<JObject>();
        }
        public dynamic GetMappedVariable(string processVariableName, Type t)
        {
            object value = null;
            if (t.IsValueType)
                value = Activator.CreateInstance(t);
            var methodInfo = typeof(WorkflowProcessInstance).GetMethod("GetVariable");
            if (methodInfo != null)
                value = methodInfo.MakeGenericMethod(t).Invoke(
                    (WorkflowProcessInstance)WorkflowProcessInstance, new object[]
                    {
                        processVariableName
                    });
            return value;
        }
        private void InitializeWorkingSet(dynamic field)
        {
            if (field == null) return;
            var fieldJArray = field as JArray;
            if (fieldJArray != null)
            {
                foreach (var obj in fieldJArray)
                {
                    InitializeWorkingSet(obj);
                }
            }
            else if (field is JObject)
            {
                var fieldJObj = (JObject)field;
                //Map the top level output tvs to pvs
                foreach (var jProperty in fieldJObj.Properties())
                {
                    // check if property is an object and contains is_mapped 
                    var propertyVal = jProperty.Value;
                    if (!(propertyVal is JArray) && !(propertyVal is JObject))
                    {
                        continue;
                    }
                    var propertyValAsJObj = propertyVal as JObject;
                    if (propertyValAsJObj != null)
                    {
                        // check if the jobject has is_mapped
                        if (!propertyValAsJObj.ContainsKey("is_mapped")) continue;
                        BHWorkflowTaskMappableVariable<JToken> propertyValAsMappableProp = null;
                        try
                        {
                            propertyValAsMappableProp = propertyValAsJObj
                                .ToObject<BHWorkflowTaskMappableVariable<JToken>>();
                        }
                        catch
                        {
                            continue; // not mappable go to next property
                        }
                        if (propertyValAsMappableProp.IsMapped)
                        {
                            var mappedVal =WorkflowProcessInstance.GetProcessVariable(
                                    propertyValAsMappableProp.ProcessVariable);
                            jProperty.Value = JToken.FromObject(mappedVal); // move the value up
                        }
                        else
                        {
                            jProperty.Value = propertyValAsMappableProp.Value; // move the value up
                        }
                        InitializeWorkingSet(jProperty.Value);
                    }
                    else // JArray
                    {
                        InitializeWorkingSet(jProperty.Value); // if null then could be JArray
                    }
                }
            }
        }

    }
}