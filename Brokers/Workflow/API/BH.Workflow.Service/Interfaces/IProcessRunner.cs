﻿namespace BH.ScalableServices.Brokers.Workflow.Interfaces
{
    public interface IProcessRunner
    {
        void RunProcess(string runId, string args);
    }
}
