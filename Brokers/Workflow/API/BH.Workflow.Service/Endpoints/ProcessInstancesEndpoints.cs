﻿using System;
using System.Collections.Generic;
using System.IO;
using BH.ScalableServices.Brokers.Workflow.Handlers;
using BH.ScalableServices.Brokers.Workflow.Shared.Handlers;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using Nancy;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Workflow.Endpoints
{
    public class ProcessInstancesEndpoints : ResponseHandlerNancyModule
    {
        private string routeVersion = "v2.0";
        public IProcessInstanceRepository ProcessInstanceRepo { get; set; }

        public ProcessInstancesEndpoints(IProcessInstanceRepository piRepo)
        {
            ProcessInstanceRepo = piRepo;

            Get[routeVersion + "/processInstances"] = parameters =>
            {
                try
                {
                    var processInstanceName = Request.Query["process_instance_name"].Value;
                    if (processInstanceName != null)
                    {
                        var processInstance = (WorkflowProcessInstance)GetProcessInstance(processInstanceName);
                        return Response.AsJson(processInstance).WithStatusCode(HttpStatusCode.OK);
                    }
                    else
                    {
                        var processInstances = GetProcessInstances();
                        return Response.AsJson(processInstances).WithStatusCode(HttpStatusCode.OK);

                    }
                    // return HandleResponse("The process_instance_name parameter is missing. Example: /processInstances?process_instance_name=[process_instance_name].", HttpStatusCode.BadRequest);

                }
                catch (Exception ex)
                {

                    return HandleResponse(ex);
                }
            };

            Post[routeVersion + "/processInstances"] = parameters =>
            {
                try
                {
                    var processInstanceName = Request.Query["process_instance_name"].Value;
                    if (processInstanceName != null)
                    {

                        string body;
                        using (var reader = new StreamReader(Request.Body))
                        {
                            body = reader.ReadToEnd();
                        }

                        var processInstance = JsonConvert.DeserializeObject<WorkflowProcessInstance>(body);

                        PostProcessInstance(processInstanceName, processInstance);

                        return HttpStatusCode.OK;
                    }

                    return
                        HandleResponse(
                            "The process_instance_name parameter is missing. Example: /processInstances?process_instance_name=[process_instance_name].",
                            HttpStatusCode.BadRequest);

                }
                catch (JsonReaderException jex)
                {
                    return HandleResponse("There was an issue parsing the request json body.  Make sure the json is a valid Process Instance - Exception: " + jex, HttpStatusCode.BadRequest);
                }
                catch (Exception ex)
                {

                    return HandleResponse(ex);
                }
            };

            Delete[routeVersion + "/processInstances"] = parameters =>
            {
                try
                {
                    var processInstanceName = Request.Query["process_instance_name"].Value;
                    if (processInstanceName != null)
                    {
                        DeleteProcessInstance(processInstanceName);

                        return HttpStatusCode.OK;
                    }
                    else
                    {
                        string body;
                        using (var reader = new StreamReader(Request.Body))
                        {
                            body = reader.ReadToEnd();
                        }

                        var processInstance = JsonConvert.DeserializeObject<WorkflowProcessInstance>(body);

                        DeleteProcessInstance(processInstance.Name);
                    }

                    return
                        HandleResponse(
                            "The process_instance_name parameter is missing. Example: /processInstances?process_instance_name=[process_instance_name].",
                            HttpStatusCode.BadRequest);

                }
                catch (JsonReaderException jex)
                {
                    return HandleResponse("There was an issue parsing the request json body.  Make sure the json is a valid Process Instance - Exception: " + jex, HttpStatusCode.BadRequest);
                }
                catch (Exception ex)
                {

                    return HandleResponse(ex);
                }
            };

        }
        private IWorkflowProcessInstance GetProcessInstance(string name)
        {
            var processInstance = ProcessInstanceRepo.GetItem(name);

            if (processInstance == null)
               throw new WorkflowException("The Process Instance could not be found.", HttpStatusCode.NotFound);

            return processInstance;
        }
        private IEnumerable<IWorkflowProcessInstance> GetProcessInstances()
        {
            var processInstances = ProcessInstanceRepo.GetItems();

            if (processInstances == null)
                throw new WorkflowException("The Process Instances could not be found.", HttpStatusCode.NotFound);

            return processInstances;
        }
        private void PostProcessInstance(string name, IWorkflowProcessInstance processInstance)
        {
            ProcessInstanceRepo.AddUpdate(name, processInstance);
        }
        private void DeleteProcessInstance(string name)
        {
            ProcessInstanceRepo.Remove(name);
        }
    }
}
