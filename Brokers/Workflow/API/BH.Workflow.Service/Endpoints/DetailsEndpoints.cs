﻿using System.IO;
using BH.ScalableServices.Brokers.APIBase.Endpoints;
using BH.ScalableServices.Brokers.APIBase.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;

namespace BH.ScalableServices.Brokers.Workflow.Endpoints
{
    /// <summary>
    /// 
    /// </summary>
    public class WFSDetailsEndpoints : DetailsEndpoints
    {
        public WFSDetailsEndpoints(IRunDetailsRepository runRepository,WorkflowSettings settings) : base(runRepository)
        {
            runRepository.RunsFolder = Path.Combine(settings.ProcessRunsLocation);
        }


        protected override string RouteVersion { get { return "v2.0"; } }
    }


}
