﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BH.ScalableServices.Brokers.Workflow.Handlers;
using BH.ScalableServices.Brokers.Workflow.Shared.Handlers;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using Nancy;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Workflow.Endpoints
{
    public class TaskInstancesEndpoints : ResponseHandlerNancyModule
    {
        private string routeVersion = "v2.0";
        public ITaskInstanceRepository TaskInstanceRepo { get; set; }
        
        public TaskInstancesEndpoints(ITaskInstanceRepository taskInstanceRepo)
        {
            TaskInstanceRepo = taskInstanceRepo;

            Get[routeVersion + "/taskInstances"] = parameters =>
            {
                try
                {
                    var processInstanceName = Request.Query["process_instance_name"].Value;
                    var taskInstanceName = Request.Query["task_instance_name"].Value;

                
                    if (processInstanceName != null && taskInstanceName != null)
                    {
                        var processInstance = (WorkflowTaskInstance) GetTaskInstance(processInstanceName, taskInstanceName);
                        return Response.AsJson(processInstance).WithStatusCode(HttpStatusCode.OK);
                    }
                    
                    if (processInstanceName != null)
                    {
                        var processInstance = (List<IWorkflowTaskInstance>) GetTaskInstances(processInstanceName);
                        return Response.AsJson(processInstance).WithStatusCode(HttpStatusCode.OK);
                    }
                    
                    throw new WorkflowException("Missing parameters process_instance_name and/or task_instance_name.  Example: /taskInstances?process_instance_name=[process_instance_name]&task_instance_name=[task_instance_name]", HttpStatusCode.BadRequest);
                }
                catch (JsonReaderException jex)
                {
                    return HandleResponse("There was an issue parsing the request json body.  Make sure the json is a valid Task Instance - Exception: " + jex, HttpStatusCode.BadRequest);
                }
                catch (Exception ex)
                {

                    return HandleResponse(ex);
                }
            };

            Post[routeVersion + "/taskInstances"] = parameters =>
            {
                try
                {
                    var processInstanceName = Request.Query["process_instance_name"].Value;
                    var taskInstanceName = Request.Query["task_instance_name"].Value;
                    if (processInstanceName != null && taskInstanceName != null)
                    {

                        string body;
                        using (var reader = new StreamReader(Request.Body))
                        {
                            body = reader.ReadToEnd();
                        }

                        //is an object
                        if (body.Trim().StartsWith("{"))
                        {
                            var taskInstance = JsonConvert.DeserializeObject<WorkflowTaskInstance>(body);

                            PostTaskInstance(processInstanceName, taskInstanceName, taskInstance);
                        }
                        else
                        {
                            var taskInstances = JsonConvert.DeserializeObject<List<WorkflowTaskInstance>>(body);

                            foreach (var taskInstance in taskInstances) 
                                PostTaskInstance(processInstanceName, taskInstance.Name, taskInstance);
                        }

                        return HttpStatusCode.OK;
                    }

                    if (processInstanceName != null)
                    {

                        string body;
                        using (var reader = new StreamReader(Request.Body))
                        {
                            body = reader.ReadToEnd();
                        }

                        //is an object
                        if (body.Trim().StartsWith("["))
                        {
                            var taskInstances = JsonConvert.DeserializeObject<List<WorkflowTaskInstance>>(body);

                            foreach (var taskInstance in taskInstances)
                                PostTaskInstance(processInstanceName, taskInstance.Name, taskInstance);

                            return HttpStatusCode.OK;
                        }
                        else
                        {
                            throw new WorkflowException("Missing parameters task_instance_name.  Example: /taskInstances?process_instance_name=[process_instance_name]&task_instance_name=[task_instance_name]", HttpStatusCode.BadRequest);

                        }
                    }

                    throw new WorkflowException("Missing parameters process_instance_name.  Example: /taskInstances?process_instance_name=[process_instance_name]&task_instance_name=[task_instance_name]", HttpStatusCode.BadRequest);

                }
                catch (JsonReaderException jex)
                {
                    return HandleResponse("There was an issue parsing the request json body.  Make sure the json is a valid Task Instance - Exception: " + jex, HttpStatusCode.BadRequest);
                }
                catch (Exception ex)
                {

                    return HandleResponse(ex);
                }
            };








            Delete[routeVersion + "/taskInstances"] = parameters =>
            {
                try
                {
                    var processInstanceName = Request.Query["process_instance_name"].Value;
                    var taskInstanceName = Request.Query["task_instance_name"].Value;
                    if (processInstanceName != null && taskInstanceName != null)
                    {
                        DeleteTaskInstance(processInstanceName, taskInstanceName);

                        return HttpStatusCode.OK;
                    }

                    if (processInstanceName != null)
                    {

                        string body;
                        using (var reader = new StreamReader(Request.Body))
                        {
                            body = reader.ReadToEnd();
                        }

                        //is an object
                        if (body.Trim().StartsWith("["))
                        {
                            var taskInstances = JsonConvert.DeserializeObject<List<WorkflowTaskInstance>>(body);

                            foreach (var taskInstance in taskInstances)
                                DeleteTaskInstance(processInstanceName, taskInstance.Name);

                            return HttpStatusCode.OK;
                        }
                        else if (body.Trim().StartsWith("{"))
                        {
                            var taskInstance = JsonConvert.DeserializeObject<WorkflowTaskInstance>(body);

                            DeleteTaskInstance(processInstanceName, taskInstance.Name);

                            return HttpStatusCode.OK;
                        }
                        else
                        {
                            throw new WorkflowException("Missing parameters task_instance_name or missing body.  Example: /taskInstances?process_instance_name=[process_instance_name]&task_instance_name=[task_instance_name]", HttpStatusCode.BadRequest);
                        }
                    }

                    throw new WorkflowException("Missing parameters process_instance_name.  Example: /taskInstances?process_instance_name=[process_instance_name]&task_instance_name=[task_instance_name]", HttpStatusCode.BadRequest);

                }
                catch (JsonReaderException jex)
                {
                    return HandleResponse("There was an issue parsing the request json body.  Make sure the json is a valid Task Instance - Exception: " + jex, HttpStatusCode.BadRequest);
                }
                catch (Exception ex)
                {

                    return HandleResponse(ex);
                }
            };

        }
        
        private IWorkflowTaskInstance GetTaskInstance(string processInstanceName, string taskInstanceName)
        {
            var processInstance = TaskInstanceRepo.GetItem(processInstanceName, taskInstanceName);

            if (processInstance == null)
               throw new WorkflowException("The Task Instance could not be found.", HttpStatusCode.NotFound);

            return processInstance;
        }

        private List<IWorkflowTaskInstance> GetTaskInstances(string processInstanceName)
        {
            var taskInstances = TaskInstanceRepo.GetItems(processInstanceName);

            if (taskInstances == null || !taskInstances.Any())
                throw new WorkflowException("There are no Task Instances found for the given process_instance_name: " + processInstanceName, HttpStatusCode.NotFound);

            return taskInstances;
        }
        private void PostTaskInstance(string processInstanceName, string taskInstanceName, WorkflowTaskInstance taskInstance)
        {
            if (taskInstanceName.ToLower() == "processinstance" || taskInstance.Name.ToLower() == "processinstance")
                throw new WorkflowException("The Task Instance Name is not allowed to be processinstance because its a reserved name.", HttpStatusCode.BadRequest);

            if (string.IsNullOrEmpty(taskInstance.TaskSchemaName))
                throw new WorkflowException("Task Instance json body doesn't contain a task_schema_name.  Please use a task_schema_name and try again.", HttpStatusCode.BadRequest);
            
            TaskInstanceRepo.AddUpdate(processInstanceName, taskInstanceName, taskInstance);
        }

        private void DeleteTaskInstance(string processInstanceName, string taskInstanceName)
        {
            if (taskInstanceName.ToLower() == "processinstance")
                throw new WorkflowException("The Task Instance Name is not allowed to be processinstance because its a reserved name.", HttpStatusCode.BadRequest);

            TaskInstanceRepo.Remove(processInstanceName, taskInstanceName);
        }
    }



}
