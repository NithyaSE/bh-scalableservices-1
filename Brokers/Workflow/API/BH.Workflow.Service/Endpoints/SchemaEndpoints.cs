﻿using System.Collections.Generic;
using System.Dynamic;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using Nancy;

namespace BH.ScalableServices.Brokers.Workflow.Endpoints
{
    /// <summary>
    /// 
    /// </summary>
    public class SchemaEndpoints : NancyModule
    {
        private string routeVersion = "v2.0";
        private ITaskSchemaRepository TaskSchemasRepo { get; set; }

        private IProcessSchemaRepository ProcessSchemasRepo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public SchemaEndpoints(ITaskSchemaRepository taskSchemasRepo, IProcessSchemaRepository processSchemaRepo)
        {
            TaskSchemasRepo = taskSchemasRepo;
            ProcessSchemasRepo = processSchemaRepo;
            //Used by configuration
            Get[routeVersion + "/schemas"] = parameters =>
            {
                var schemaNames = new List<string>();
                var schemas = taskSchemasRepo.GetItems();
                foreach (var schema in schemas)
                {
                    schemaNames.Add(schema.name);
                }


                return Response.AsJson(schemaNames).WithStatusCode(HttpStatusCode.OK);
            };

            Get[routeVersion + "/schema/{name}"] = parameters =>
            {
                var schemaName = parameters.name.ToString();
                
                var schema = schemaName.ToLower() == "processinstance" ? 
                         processSchemaRepo.GetItem() : taskSchemasRepo.GetItem(schemaName);

                return Response.AsJson((ExpandoObject)schema).WithStatusCode(HttpStatusCode.OK);
            };

            Get[routeVersion + "/TaskFlavors"] = parameters =>
            {
                var schemaNames = new List<string>();
                var schemas = taskSchemasRepo.GetItems();
                foreach (var schema in schemas)
                {
                    schemaNames.Add(schema.name);
                }


                return Response.AsJson(schemaNames).WithStatusCode(HttpStatusCode.OK);
            };

            Get[routeVersion + "/TaskConfigSchema/{TaskFlavor}"] = parameters =>
            {
                var schemaName = parameters.TaskFlavor.ToString();

                var schema = schemaName.ToLower() == "processinstance" ?
                         processSchemaRepo.GetItem() : taskSchemasRepo.GetItem(schemaName);

                return Response.AsJson((ExpandoObject)schema).WithStatusCode(HttpStatusCode.OK);
            };

        }


    }

}
