﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using Nancy;

namespace BH.ScalableServices.Brokers.Workflow.Endpoints
{
    public class UploadEndpoints : NancyModule
    {
        private string routeVersion = "v2.0";
        private WorkflowSettings Settings { get; set; }
        public UploadEndpoints(WorkflowSettings settings)
        {
            Settings = settings;
            Settings.LoadSettings(Path.GetFileName(Assembly.GetEntryAssembly().CodeBase));

            Post[routeVersion + "/upload/files"] = parameters =>
            {
                try
                {
                    var uploadFilesPath = Settings.UploadsLocation; 

                    if (uploadFilesPath != "")
                    {
                        uploadFilesPath = uploadFilesPath.EndsWith("\\") ? uploadFilesPath : uploadFilesPath + "\\";
                    }

                    foreach (var file in Request.Files)
                    {
                        var fileContent = file.Value;
                        if (fileContent != null && fileContent.Length > 0)
                        {
                            // get a stream
                            // and optionally write the file to disk
                            var fileName = file.Name.Contains(';') ? file.Name.Split(';')[0] : file.Name;
                            var fullFileName = Path.Combine(uploadFilesPath, fileName);
                            using (var fileStream = File.Create(fullFileName))
                            {
                                fileContent.CopyTo(fileStream);
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    return HttpStatusCode.BadRequest;

                }

                return HttpStatusCode.Accepted;
            };

            Post[routeVersion + "/upload/tasks"] = parameters =>
            {
                try
                {
                    var TaskFilesPath = Settings.TasksLocation;

                    if (TaskFilesPath != "")
                    {
                        TaskFilesPath = TaskFilesPath.EndsWith("\\") ? TaskFilesPath : TaskFilesPath + "\\";
                    }

                    foreach (var file in Request.Files)
                    {
                        var fileContent = file.Value;
                        if (fileContent != null && fileContent.Length > 0)
                        {
                            // get a stream
                            // and optionally write the file to disk
                      
                            var fileName = file.Name.Contains(';') ? file.Name.Split(';')[0] : file.Name;
                            var fullFileName = Path.Combine(TaskFilesPath, fileName);
                            using (var fileStream = File.Create(fullFileName))
                            {
                                fileContent.CopyTo(fileStream);
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    return HttpStatusCode.BadRequest;

                }

                return HttpStatusCode.Accepted;
            };

            Post[routeVersion + "/upload/schemas"] = parameters =>
            {
                try
                {
                    var schemaFilesPath = Settings.SchemasLocation; 

                    if (schemaFilesPath != "")
                    {
                        schemaFilesPath = schemaFilesPath.EndsWith("\\") ? schemaFilesPath : schemaFilesPath + "\\";
                    }

                    foreach (var file in Request.Files)
                    {
                        var fileContent = file.Value;
                        if (fileContent != null && fileContent.Length > 0)
                        {
                            // get a stream
                            // and optionally write the file to disk
                            var fileName = file.Name.Contains(';') ? file.Name.Split(';')[0] : file.Name;
                            var fullFileName = Path.Combine(schemaFilesPath, fileName);
                            using (var fileStream = File.Create(fullFileName))
                            {
                                fileContent.CopyTo(fileStream);
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    return HttpStatusCode.BadRequest;

                }

                return HttpStatusCode.Accepted;
            };
        }
    }
}
