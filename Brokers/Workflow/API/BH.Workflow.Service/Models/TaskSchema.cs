﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Workflow.Models
{
    public class TaskSchema
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

    }
}
