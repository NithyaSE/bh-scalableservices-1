﻿using System;
using System.Configuration;
using System.IO;
using System.Text;
using SystemWrapper.IO;
using BH.ScalableServices.Brokers.Workflow.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Models;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Responses;
using Nancy.TinyIoc;
using Serilog;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Respositories;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using BH.ScalableServices.Brokers.APIBase.Interfaces;
using BH.ScalableServices.Brokers.APIBase.Repositories;
using System.Reflection;
using Shared.SerilogsLogging;
using Shared.ServerLibrary;
using Shared.Tools;

namespace BH.ScalableServices.Brokers.Workflow
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        #region Properties
        public bool UseAuthHeader { get; set; }
        private static string Username
        {
            get { return GenTools.Instance.GetEnvironmentVariable(EnvironmentVariables.BHWFSApiCredUsername); }
        }
        private static string Password
        {
            get { return GenTools.Instance.GetEnvironmentVariable(EnvironmentVariables.BHWFSApiCredPassword); }
        }
        public static TinyIoCContainer Container { get; set; }
        #endregion
        #region Constructors
        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            base.ConfigureApplicationContainer(container);
            //Register any dependencies for modules
            container.Register<IRunDetailsRepository, RunDetailsFileRepository>();
            container.Register<IProcessInstanceRepository, ProcessInstanceFileRepository>();
            container.Register<ITaskInstanceRepository, TaskInstanceFileRepository>();
            container.Register<IProcessSchemaRepository, ProcessSchemaFileRepository>();
            container.Register<ITaskSchemaRepository, TaskSchemaFileRepository>();
            container.Register<IProcessOutputsRepository, ProcessOutputsFileRepository>();
            container.Register<IRelocator<IWorkflowTaskInstance>, TaskInstanceRelocator>();
            container.Register<IRelocator<IWorkflowProcessInstance>, ProcessInstanceRelocator>();
            container.Register<IProcessRunner, ProcessRunner>();
            container.Register<IFileWrap, FileWrap>();
            container.Register<IDirectoryWrap, DirectoryWrap>();
            var settings = new WorkflowSettings();
            container.Register(settings);
            Setup(settings);
            container.Register<IProcessRunsRepository, ProcessRunsRepository>();
            Container = container;
        }
        protected override void ConfigureRequestContainer(TinyIoCContainer container, NancyContext context)
        {
            var repo = container.Resolve<IRunDetailsRepository>();
            var settings = container.Resolve<WorkflowSettings>();
            var run_id = context.Request.Query["run_id"].Value;
            if(!string.IsNullOrEmpty(run_id))
                repo.RunsFolder =  Path.Combine(settings.ProcessRunsLocation);
        }
        private void Setup(WorkflowSettings settings)
        {
            try
            {
                settings.LoadSettings(Path.GetFileName(Assembly.GetEntryAssembly().CodeBase));
            }
            catch (Exception ex)
            {
                Log.Logger.AddMethodName().Error("Summary='Critical Workflow Service Error : Error in Initial Setup' Details='{@ex}'",ex);
            }
        }
        public Bootstrapper() : base()
        {
            UseAuthHeader = Convert.ToBoolean(ConfigurationManager.AppSettings["UseAuthHeader"]);
        }

        //Can inject whether or not to use auth header authentication
        public Bootstrapper(bool useAuthHeader) : base()
        {
            UseAuthHeader = useAuthHeader;
        }
        #endregion
        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            StaticConfiguration.DisableErrorTraces = false;
            base.ApplicationStartup(container, pipelines);
            Console.WriteLine(@"Enabled Error Tracing...");
        }
        // The bootstrapper enables you to reconfigure the composition of the framework,
        // by overriding the various methods and properties.
        // For more information https://github.com/NancyFx/Nancy/wiki/Bootstrapper

        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            if (UseAuthHeader)
            {
                pipelines.BeforeRequest.AddItemToEndOfPipeline((ctx) =>
                {
                    var authHeader = ctx.Request.Headers.Authorization;
                    var authenticated = authHeader ==
                                        "Basic " +
                                        Convert.ToBase64String(
                                            Encoding.UTF8.GetBytes(Username + ":" + Password));

                    return authenticated ? null : new HtmlResponse(HttpStatusCode.Unauthorized);
                });
            }
            pipelines.AfterRequest.AddItemToEndOfPipeline((ctx) =>
                {
                    ctx.Response.WithHeader("Access-Control-Allow-Origin", "*")
                                .WithHeader("Access-Control-Allow-Methods", "POST,GET")
                                .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type");

                });
            pipelines.OnError += (ctx, ex) =>
            {
                var errorMsg = "Error occurred while handling the following request : " + ctx.Request.Method + " " + ctx.Request.Path;
                Log.Logger.AddMethodName().Error("'Summary=Errors {@err}' Details='{ex}'",errorMsg,ex);
                return null;
            };
        }
    }
   
}