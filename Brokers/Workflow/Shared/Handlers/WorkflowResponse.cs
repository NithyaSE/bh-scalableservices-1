﻿using Nancy;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Handlers
{

    public class WorkflowResponse
    {
        public WorkflowResponse()
        {
            Error = null;
        }
        
        [JsonProperty("status_code")]
        public HttpStatusCode StatusCode { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }

    }

}
