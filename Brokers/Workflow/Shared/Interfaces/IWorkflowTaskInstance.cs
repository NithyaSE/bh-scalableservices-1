﻿using System.Collections.Generic;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Interfaces
{
    public interface IWorkflowTaskInstance
    {
        [JsonProperty("name")]
        string Name { get; set; }
        [JsonProperty("working_set")]
        JObject WorkingSet { get; set; }
        [JsonProperty("schema_name")]
        string TaskSchemaName { get; set; }
        [JsonProperty("is_active")]
        BHWorkflowTaskMappableVariable<bool> IsActive { get; set; }
        [JsonProperty("working_set_input_mappings_to_process_variables")]
        JObject WorkingSetInputMappingsToPvs { get; set; }
        [JsonProperty("working_set_output_mappings_to_process_variables")]
        IDictionary<string, string> WorkingSetOutputMappingsToPvs { get; set; } // output property name and pv name
        [JsonProperty("system_task_variables")]
        JObject SystemTaskVariables { get; set; }
        [JsonProperty("dll_name")]
        string TaskDllName { get; }

    }
}
