﻿namespace BH.ScalableServices.Brokers.Workflow.Shared.Interfaces
{

    public interface IProcessSchemaRepository
    {

        dynamic GetItem();

    }
}
