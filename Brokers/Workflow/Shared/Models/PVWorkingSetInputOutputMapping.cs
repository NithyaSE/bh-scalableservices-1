﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Models
{
    public class PVWorkingSetInputOutputMapping
    {
        [JsonProperty("process_variable_name")]
        public string ProcessVariableName { get; set; }

        [JsonProperty("output_process_variable_name")]
        public string OutputProcessVariableName { get; set; }
    }
}
