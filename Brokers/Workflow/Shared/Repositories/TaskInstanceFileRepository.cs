﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using SystemWrapper.IO;
using Nancy;
using Newtonsoft.Json;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using BH.ScalableServices.Brokers.Workflow.Shared.Handlers;
using System.Reflection;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Respositories
{
    public class TaskInstanceFileRepository : ITaskInstanceRepository
    {
        #region Properties
        public IFileWrap FileIO { get; set; }

        public IDirectoryWrap DirIO { get; set; }

        public IRelocator<IWorkflowTaskInstance> Relocator { get; set; }

        public WorkflowSettings Settings { get; set; }

        #endregion

        #region Constructors
        public TaskInstanceFileRepository(IRelocator<IWorkflowTaskInstance> relocator, IFileWrap fileWrap, IDirectoryWrap dirWrap, WorkflowSettings settings)
        {
            Relocator = relocator;
            FileIO = fileWrap;
            DirIO = dirWrap;
            Settings = settings;
            Settings.LoadSettings(Path.GetFileName(Assembly.GetEntryAssembly().CodeBase));
        }
        #endregion

        #region Public Repository Methods

        public void Add(string processInstanceName, IWorkflowTaskInstance taskInstance)
        {
            var processPath = Path.Combine(Settings.ProcessBundlesLibraryLocation, processInstanceName);
            if (!DirIO.Exists(processPath))
                throw new WorkflowException("The Process Instance cannot be found.", HttpStatusCode.NotFound);

            var taskInstancePath = processPath + "\\" + taskInstance.TaskSchemaName + "_" + taskInstance.Name + ".json";

            var taskInstanceJson = JsonConvert.SerializeObject(taskInstance);
            FileIO.WriteAllText(taskInstancePath, taskInstanceJson);
        }

        public void AddUpdate(string processInstanceName, string taskInstanceName, IWorkflowTaskInstance taskInstance)
        {
            //handles renaming/relocating files
            if (taskInstanceName != taskInstance.Name)
            {
                var taskSchemaName = taskInstance.TaskSchemaName;
                var oldPath = Path.Combine(processInstanceName, taskSchemaName + "_" + taskInstanceName);
                var newPath = Path.Combine(processInstanceName, taskSchemaName + "_" + taskInstance.Name);

                Relocator.Relocate<WorkflowTaskInstance>(oldPath, newPath);
            }

            Remove(processInstanceName, taskInstance.Name);
            Add(processInstanceName, taskInstance);
        }

        public void Remove(string processInstanceName, string taskInstanceName)
        {
            var processPath = Path.Combine(Settings.ProcessBundlesLibraryLocation, processInstanceName);
            if (!DirIO.Exists(processPath)) return;

            var instanceFiles = DirIO.GetFiles(processPath, "*.json").ToList();
            foreach (var instanceFile in instanceFiles)
            {
                if (instanceFile != null && !instanceFile.ToLower().EndsWith("processinstance.json") && instanceFile.EndsWith(taskInstanceName + ".json"))
                {
                    FileIO.Delete(instanceFile);
                }
            }
        }

        public IWorkflowTaskInstance GetItem(string processInstanceName, string taskInstanceName)
        {
            var processContextPath = Path.Combine(Settings.ProcessBundlesLibraryLocation, processInstanceName);
            WorkflowTaskInstance instance = null;
            if (!DirIO.Exists(processContextPath)) return null;

            var instanceFilePattern = "*_" + taskInstanceName + ".json";

            var instanceLoc = DirIO.GetFiles(processContextPath, instanceFilePattern).FirstOrDefault();
            if (instanceLoc == null) return instance;
            var instanceJson = FileIO.ReadAllText(instanceLoc);
            instance = JsonConvert.DeserializeObject<WorkflowTaskInstance>(instanceJson);

            return instance;

        }

        public List<IWorkflowTaskInstance> GetItems(string processInstanceName)
        {
            var processContextPath = Path.Combine(Settings.ProcessBundlesLibraryLocation, processInstanceName);
            var instances = new List<IWorkflowTaskInstance>();
            if (!DirIO.Exists(processContextPath)) return null;

            var instanceFiles = DirIO.GetFiles(processContextPath, "*.json").ToList();
            foreach (var instanceFile in instanceFiles)
            {
                if (instanceFile != null && !instanceFile.ToLower().EndsWith("processinstance.json"))
                {
                    var instanceJson = FileIO.ReadAllText(instanceFile);
                    var instance = JsonConvert.DeserializeObject<WorkflowTaskInstance>(instanceJson);
                    instances.Add(instance);
                }
            }
            return instances;
        }

        #endregion

    }



}
