﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using SystemWrapper.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using System.Reflection;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Respositories
{
    public class TaskSchemaFileRepository : ITaskSchemaRepository
    {
        #region Properties
        public IFileWrap FileIO { get; set; }

        public IDirectoryWrap DirIO { get; set; }

        public WorkflowSettings Settings { get; set; }

        private const string SchemaFileNamePostFix = "_schema.json";
        #endregion

        #region Constructors
        public TaskSchemaFileRepository(IFileWrap fileWrap, IDirectoryWrap dirWrap, WorkflowSettings settings)
        {
            FileIO = fileWrap;
            DirIO = dirWrap;
            Settings = settings;
            Settings.LoadSettings(Path.GetFileName(Assembly.GetEntryAssembly().CodeBase));
        }


        #endregion

        public void Add(dynamic item)
        {
            throw new NotImplementedException();
        }

        public void AddUpdate(string name, dynamic schema)
        {
            DeleteSchemaJson(name);
            AddSchemaJson(name, schema);
        }

        private void AddSchemaJson(string name, dynamic schema)
        {
            var filename = name + SchemaFileNamePostFix;
            if (schema != null && schema.GetType().GetProperty("name") != null)
                filename = schema.name + SchemaFileNamePostFix;
            else if (schema.GetType() == typeof(JObject))
            {
                var JObj = ((JObject)schema);
                foreach (JToken token in JObj.Children())
                {
                    if (token is JProperty)
                    {
                        var prop = token as JProperty;
                        if (prop.Name == "name")
                            filename = prop.Value + SchemaFileNamePostFix;
                    }
                }
            }

            FileIO.WriteAllText(Path.Combine(Settings.SchemasLocation, filename), JsonConvert.SerializeObject(schema));
        }

        public void Remove(string taskSchemaName)
        {
            DeleteSchemaJson(taskSchemaName);
        }

        public dynamic GetItem(string taskSchemaName)
        {
            var json = GetSchemaJson(taskSchemaName);
            if (string.IsNullOrEmpty(json)) return null;

            var taskSchema = JsonConvert.DeserializeObject<ExpandoObject>(json);
            return taskSchema;

        }

        public string GetItemAsJson(string taskSchemaName)
        {
            return GetSchemaJson(taskSchemaName);
        }

        public List<dynamic> GetItems()
        {
            var scheamFiles = GetAllSchemaFiles();
            var schemaJsonList = new List<dynamic>();
            foreach (var schemaFile in scheamFiles)
            {
                var schemaJson = FileIO.ReadAllText(schemaFile);
                var schema = JsonConvert.DeserializeObject<ExpandoObject>(schemaJson);
                schemaJsonList.Add(schema);
            }
            return schemaJsonList;
        }

        public List<dynamic> GetItems(string property)
        {
            var scheamFiles = GetAllSchemaFiles();
            var schemaJsonList = new List<dynamic>();
            foreach (var schemaFile in scheamFiles)
            {
                var fileName = Path.GetFileName(schemaFile);
                if (fileName != null && fileName.Contains(property))
                {
                    var schemaJson = FileIO.ReadAllText(schemaFile);
                    var schema = JsonConvert.DeserializeObject<ExpandoObject>(schemaJson);
                    schemaJsonList.Add(schema);
                }
            }
            return schemaJsonList;
        }


        private string GetSchemaJson(string schemaName)
        {
            var schemas = GetAllSchemaFiles();
            foreach (var schema in schemas)
            {
                var fileName = Path.GetFileName(schema);
                if (fileName == null) continue;

                var schemaFileName = fileName.Replace(SchemaFileNamePostFix, "");
                if (schemaFileName != schemaName) continue;

                var schemaJson = FileIO.ReadAllText(schema);
                return schemaJson;
            }

            return null;
        }

        private List<string> GetAllSchemaFiles()
        {
            var schemaFiles = new List<string>();

            if (!DirIO.Exists(Settings.SchemasLocation)) return schemaFiles;

            var schemas = DirIO.GetFiles(Settings.SchemasLocation, "*" + SchemaFileNamePostFix);
            foreach (var schema in schemas)
            {
                schemaFiles.Add(schema);
            }

            return schemaFiles;
        }

        private void DeleteSchemaJson(string schemaName)
        {
            var schemas = GetAllSchemaFiles();
            foreach (var schema in schemas)
            {
                var fileName = Path.GetFileName(schema);
                if (fileName == null) continue;

                var schemaFileName = fileName.Replace(SchemaFileNamePostFix, "");
                if (schemaFileName != schemaName) continue;

                File.Delete(schema);
            }
        }


    }

}
