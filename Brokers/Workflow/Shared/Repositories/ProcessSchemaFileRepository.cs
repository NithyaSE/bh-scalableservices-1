﻿using System.Dynamic;
using SystemWrapper.IO;
using Newtonsoft.Json;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using System.Reflection;
using System.IO;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Respositories
{
    public class ProcessSchemaFileRepository : IProcessSchemaRepository
    {
        #region Properties

        public IFileWrap FileIO { get; set; }

        public IDirectoryWrap DirIO { get; set; }

        public WorkflowSettings Settings { get; set; }

        #endregion

        #region Constructors

        public ProcessSchemaFileRepository(IFileWrap fileWrap, IDirectoryWrap dirWrap, WorkflowSettings settings)
        {
            FileIO = fileWrap;
            DirIO = dirWrap;
            Settings = settings;
            Settings.LoadSettings(Path.GetFileName(Assembly.GetEntryAssembly().CodeBase));
        }


        #endregion

        public dynamic GetItem()
        {
            var json = GetItemAsJson();
            var processSchema = JsonConvert.DeserializeObject<ExpandoObject>(json);
            return processSchema;
        }

        public string GetItemAsJson()
        {
            var json = FileIO.ReadAllText("Schemas\\processinstance_schema.json");
            return json;
        }
    }

}
