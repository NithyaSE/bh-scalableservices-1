﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using SystemWrapper.IO;
using Nancy;
using Newtonsoft.Json;
using Serilog;
using BH.ScalableServices.Brokers.APIBase.Helpers;
using BH.ScalableServices.Brokers.APIBase.Interfaces;
using BH.ScalableServices.Brokers.APIBase.Models;
using BH.ScalableServices.Brokers.Shared.Models;
using Shared.SerilogsLogging;

namespace BH.ScalableServices.Brokers.APIBase.Repositories
{
    public class RunDetailsFileRepository : IRunDetailsRepository
    {
        #region Properties

        //, runId,"Logs"

        public string RunsFolder { get; set; } = "Runs";

        //private object writelock = new object();
        static ReaderWriterLock rwl = new ReaderWriterLock();

        public string LogsFolder = "RunDetails";
        public string LogEntriesFolder = "LogEntries";
        public string RunDetailsFileName = "RunDetails.json";
        public string RunStatusFileName = "Status.json";

        public IFileWrap FileIO { get; set; }

        public IDirectoryWrap DirIO { get; set; }

        #endregion

        #region Constructors

        public RunDetailsFileRepository(IFileWrap fileWrap, IDirectoryWrap dirWrap)
        {
            FileIO = fileWrap;
            DirIO = dirWrap;
        }

        #endregion

        #region Public Repository Methods


        public void AddRunDetails(ScalableServiceRunDetails runDetails)
        {
            EnsureRunFolderExists(runDetails.RunId);

            var runPath = Path.Combine(RunsFolder, runDetails.RunId, LogsFolder, RunDetailsFileName);
            var runJson = JsonConvert.SerializeObject(runDetails);

            WriteFileToRepository(runPath, runJson, runDetails.RunId);

        }

        public void AddStatus(string runId, ScalableServiceStatus status)
        {
            EnsureRunFolderExists(runId);

            if (!status.IsDehydrated.HasValue)
                status.IsDehydrated = false;
            if (!status.IsException.HasValue)
                status.IsException = false;
            if (!status.IsExecuting.HasValue)
                status.IsExecuting = false;
            if (!status.IsQueued.HasValue)
                status.IsQueued = false;
            if (!status.IsWarning.HasValue)
                status.IsWarning = false;

            var statusPath = Path.Combine(RunsFolder, runId, LogsFolder, RunStatusFileName);
            var statusJson = JsonConvert.SerializeObject(status);

            WriteFileToRepository(statusPath, statusJson,runId);

        }
        
        public void AddUpdateStatus(string runId, ScalableServiceStatus status, string processInstanceName)
        {
            var fileName = GetStatusFileName(runId);
            if (fileName != null)
                UpdateStatus(runId, status);
            else
            {
                AddStatus(runId, status);

                //var details = GetRunDetailsBase(runId);
                var details = new ScalableServiceRunDetails
                {
                    RunId = runId,
                    ProcessInstanceName = processInstanceName,
                    LogEntries = new List<ScalableServiceLogEntry>(),
                    EnqueuedTimestamp = DateTime.UtcNow
                };
                
                AddRunDetails(details);
            }
        }

        public void Remove(string runId)
        {
            RemoveDetailsFile(runId);
            RemoveStatusFile(runId);
            RemoveLogEntries(runId);
        }

        /// <summary>
        /// Gets the complete rundetails object along with status
        /// and logentries
        /// </summary>
        /// <param name="runId"></param>
        /// <returns></returns>
        public ScalableServiceRunDetails GetItem(string runId,bool includeLogEntries)
        {
            var runObj = GetRunDetailsBase(runId);
            if (runObj != null)
            {
                PopulateRunDetailsBase(runObj, includeLogEntries);
                return runObj;
            }

            return null;
        }

        public string GetItemAsJson(string id,bool includeLogEntries)
        {
            var runDetails = GetItem(id, includeLogEntries);
            return JsonConvert.SerializeObject(runDetails);
        }

        public List<ScalableServiceRunDetails> GetItems(bool includeLogEntries)
        {
            if (!DirIO.Exists(RunsFolder)) return null;

            var runs = new List<ScalableServiceRunDetails>();
            var runFiles = DirIO.GetFiles(RunsFolder, RunDetailsFileName, SearchOption.AllDirectories);

            foreach (var runFile in runFiles)
            {
                var runObj = GetRunDetailsFromFile(runFile);
                PopulateRunDetailsBase(runObj, includeLogEntries);
                runs.Add(runObj);
            }
            return runs;
        }

        public List<ScalableServiceRunDetails> GetItems(string processName, bool includeLogEntries)
        {
            if (!DirIO.Exists(RunsFolder)) return null;
            var runs = new List<ScalableServiceRunDetails>();

            var runFolders = DirIO.GetDirectories(RunsFolder, $"{processName}_*");

            foreach (var folder in runFolders)
            {
                var file = Path.Combine(folder, LogsFolder, RunDetailsFileName);
                var runObj = GetRunDetailsFromFile(file);
                PopulateRunDetailsBase(runObj,includeLogEntries);
                runs.Add(runObj);
            }


            return runs;
        }

        public void AddLogEnrty(string runId, string message, string source, DateTimeOffset time)
        {
            EnsureRunFolderExists(runId);
            var logEntriesFolder = Path.Combine(RunsFolder, runId, LogsFolder, LogEntriesFolder);
            if (!DirIO.Exists(logEntriesFolder))
                DirIO.CreateDirectory(logEntriesFolder);
            var id = Guid.NewGuid().ToString("N");

            var logEntry = new ScalableServiceLogEntry { Message = message, Source = source, Timestamp = time};

            var logPath = Path.Combine(logEntriesFolder, $"{time.ToString("yyyy-dd-MTHH-mm-ss")}_{id}.json");
            var logJson = JsonConvert.SerializeObject(logEntry);

            WriteFileToRepository(logPath, logJson,runId);

        }

        public void UpdateStatus(string runId, ScalableServiceStatus status)
        {
            ScalableServiceRunDetails runDetails = GetRunDetailsBase(runId);
            var existingStatus = GetStatus(runId);
            if (runDetails == null)
                throw new ExceptionStatus(string.Format("Run details for runId {0} could not be found.", runId),
                    HttpStatusCode.NotFound);
            if (existingStatus == null)
                throw new ExceptionStatus(string.Format("Status for runId {0} could not be found.", runId),
                    HttpStatusCode.NotFound);
            if (status.IsExecuting == true && existingStatus.IsExecuting == false && existingStatus.IsDehydrated == false )
                runDetails.ExecutionStartedTimestamp = DateTime.UtcNow;
            if (status.IsExecuting == false && existingStatus.IsExecuting == true && (status.IsDehydrated == false || !status.IsDehydrated.HasValue))
                runDetails.ExecutionEndedTimestamp = DateTime.UtcNow;

            // Queue time created on details creation in start endpoint (not anymore)
            //if (status.IsQueued == true && runDetails.Status.IsQueued == false)
            //    runDetails.EnqueuedTimestamp = DateTime.UtcNow;

            if (status.IsException.HasValue)
                existingStatus.IsException = status.IsException;
            if (status.IsExecuting.HasValue)
                existingStatus.IsExecuting = status.IsExecuting;
            if (status.IsQueued.HasValue)
                existingStatus.IsQueued = status.IsQueued;
            if (status.IsWarning.HasValue)
                existingStatus.IsWarning = status.IsWarning;
            if (status.IsDehydrated.HasValue)
                existingStatus.IsDehydrated = status.IsDehydrated;


            AddRunDetails(runDetails);
            AddStatus(runDetails.RunId, existingStatus);
            // the status object sent in the update  message at times only has a value for the parameters being  updated and null for the rest of the properties.
            // This status object is sent in the response to the user.// In order to reflect the right value of the status object after the update is done, the updated object (Existing status) is set as the value of status.
            PropertyCopier<ScalableServiceStatus, ScalableServiceStatus>.Copy(existingStatus, status);
        }

        #endregion

        #region Private Helper Methods

        private string GetRunFileName(string runId)
        {
            var path = Path.Combine(RunsFolder, runId, LogsFolder);
            if (!DirIO.Exists(path)) return null;

            var runFiles = DirIO.GetFiles(path, RunDetailsFileName);
            return runFiles.FirstOrDefault();
        }

        private string GetStatusFileName(string runId)
        {
            var path = Path.Combine(RunsFolder, runId, LogsFolder);
            if (!DirIO.Exists(path)) return null;

            var runFiles = DirIO.GetFiles(path, RunStatusFileName);
            return runFiles.FirstOrDefault();
        }


        //private bool IsSpecificRunFileWithProcessName(string file, string processName)
        //{
        //    if (file == null) return false;

        //    var fileProcessName = file.Split(new[] { "~~" }, StringSplitOptions.None)[0];
        //    if (fileProcessName == processName)
        //        return true;

        //    return false;
        //}

        private void EnsureRunFolderExists(string RunId)
        {
            Log.Logger.AddMethodName().Information("Summary='Runid-{@RunId} Check Runs Folder -{@exists}", RunId, DirIO.Exists(RunsFolder));
            if (!DirIO.Exists(RunsFolder))
            {
                DirIO.CreateDirectory(RunsFolder);
                Log.Logger.AddMethodName().Information("Summary='Created Run Folder - {@exists}'", DirIO.Exists(RunsFolder));
            }
            Log.Logger.AddMethodName().Information("Summary='Runid-{@RunId} Check RunId Folder - {@exists}",RunId, DirIO.Exists(Path.Combine(RunsFolder, RunId)));
            if (!DirIO.Exists(Path.Combine(RunsFolder, RunId)))
            {
                DirIO.CreateDirectory(Path.Combine(RunsFolder, RunId));
                Log.Logger.AddMethodName().Information("Summary='Created Run Folder - {@Exists}", DirIO.Exists(Path.Combine(RunsFolder, RunId)));
            }
            Log.Logger.AddMethodName().Information("Summary='Runid-{@RunId} Check Logs Folder - {@exists}",RunId, DirIO.Exists(Path.Combine(RunsFolder, RunId, LogsFolder)));
            if (DirIO.Exists(Path.Combine(RunsFolder, RunId, LogsFolder))) return;
            DirIO.CreateDirectory(Path.Combine(RunsFolder, RunId, LogsFolder));
            Log.Logger.AddMethodName().Information("Summary='Runid-{R@unId} Created Logs Folder - {@exists}", DirIO.Exists(Path.Combine(RunsFolder, RunId, LogsFolder)));
        }
        private ScalableServiceRunDetails GetRunDetailsFromFile(string file)
        {
            var runRawStr = ReadFileFromRepository(file);
            var runObj = JsonConvert.DeserializeObject<ScalableServiceRunDetails>(runRawStr);
            return runObj;

        }
        private ScalableServiceRunDetails GetRunDetailsBase(string runId)
        {
            var runFile = GetRunFileName(runId);
            if (runFile != null)
            {
                var runObj = GetRunDetailsFromFile(runFile);
                return runObj;
            }
            return null;
        }
        private void PopulateRunDetailsBase(ScalableServiceRunDetails runObj,bool includeLogEntries)
        {
            if (runObj == null) return;
            runObj.LogEntries = includeLogEntries ? GetLogEntries(runObj.RunId) : new List<ScalableServiceLogEntry>();
            runObj.Status = GetStatus(runObj.RunId);
            //calculate time values
            runObj.CalculateTotalTimes();
        }
        private ScalableServiceStatus GetStatus(string runId)
        {
            EnsureRunFolderExists(runId);
            var filePath = Path.Combine(RunsFolder, runId, LogsFolder, RunStatusFileName);
            var statusRawStr = ReadFileFromRepository(filePath);
            return JsonConvert.DeserializeObject<ScalableServiceStatus>(statusRawStr);
        }
        private void RemoveLogEntries(string runId)
        {
            EnsureRunFolderExists(runId);
            var logEntriesFolder = Path.Combine(RunsFolder, runId, LogsFolder, LogEntriesFolder);
            if (DirIO.Exists(logEntriesFolder))
                DeleteDirFromRepository(logEntriesFolder, true,runId);
        }
        private void RemoveDetailsFile(string runId)
        {
            var runFile = GetRunFileName(runId);
            if (runFile != null)
            {
                DeleteFileFromRepository(runFile,runId);
            }
        }
        private void RemoveStatusFile(string runId)
        {
            var statusFile = GetStatusFileName(runId);
            if (statusFile != null)
            {
                DeleteFileFromRepository(statusFile,runId);
            }
        }
        private List<ScalableServiceLogEntry> GetLogEntries(string runId)
        {
            var logs = new List<ScalableServiceLogEntry>();
            EnsureRunFolderExists(runId);
            var logEntriesFolder = Path.Combine(RunsFolder, runId, LogsFolder, LogEntriesFolder);
            if (!DirIO.Exists(logEntriesFolder)) return logs.OrderBy(x => x.Timestamp).ToList();
            var logFiles = DirIO.GetFiles(logEntriesFolder, "*.json");
            foreach (var logFile in logFiles)
            {
                var entry = GetLogEntryFromFile(logFile);
                logs.Add(entry);
            }
            return logs.OrderBy(x => x.Timestamp).ToList();
        }
        private ScalableServiceLogEntry GetLogEntryFromFile(string file)
        {
            var logRawStr = ReadFileFromRepository(file);
            var logObj = JsonConvert.DeserializeObject<ScalableServiceLogEntry>(logRawStr);
            return logObj;
        }
        private void WriteFileToRepository(string path, string contents,string runId)
        {
            Log.Logger.AddMethodName().Information("Summary='Acquiring writer lock over repository  for runId {@runId}'",runId);
            rwl.AcquireWriterLock(-1); // The thread will wait until the lock is acquired. We can add a timeout here.
            try
            {
                FileIO.WriteAllText(path, contents);
            }
            finally
            {
                // Ensure that the lock is released.
                Log.Logger.AddMethodName().Information("Summary='Releasing writer lock over repository for runId {@runId}'",runId);
                rwl.ReleaseWriterLock();
            }
        }
        private void DeleteFileFromRepository(string path, string runId)
        {
            Log.Logger.AddMethodName().Information("Summary='Acquiring writer lock over repository for runId {@runId}'",runId);
            rwl.AcquireWriterLock(-1); // The thread will wait until the lock is acquired. We can add a timeout here.
            try
            {
                FileIO.Delete(path);
            }
            finally
            {
                // Ensure that the lock is released.
                Log.Logger.AddMethodName().Information("Summary='Releasing writer lock over repository for runId {@runId}'",runId);
                rwl.ReleaseWriterLock();
            }
        }
        private void DeleteDirFromRepository(string path, bool recursive, string runId)
        {
            Log.Logger.AddMethodName().Information("Summary='Acquiring writer lock over repository for runId {@runId}'",runId);
            rwl.AcquireWriterLock(-1); // The thread will wait until the lock is acquired. We can add a timeout here.
            try
            {
                DirIO.Delete(path, recursive);
            }
            finally
            {
                // Ensure that the lock is released.
                Log.Logger.AddMethodName().Information("Summary='Releasing writer lock over repository for runId {@runId}'",runId);
                rwl.ReleaseWriterLock();
            }
        }
        private string ReadFileFromRepository(string path)
        {
            // The thread will wait until the lock is acquired. We can add a timeout here.
            Log.Logger.AddMethodName().Information("Summary='Acquiring reader lock over repository'");
            rwl.AcquireReaderLock(-1);
            try
            {
                // It is safe for this thread to read from the shared resource.
                return FileIO.ReadAllText(path);
            }
            finally
            {
                Log.Logger.AddMethodName().Information("Summary='Releasing reader lock over repository'");
                // Ensure that the lock is released.
                rwl.ReleaseReaderLock();
            }
        }
        #endregion
    }
}

  