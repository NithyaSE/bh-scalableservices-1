﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Shared.Models // update namespace
{
    public class ScalableServiceLogEntry // rename to scalableservicesLogEntry
    {
    [Description("The timestamp.now of when the LogEntry was created")]
    [JsonProperty("timestamp")]
    public DateTimeOffset Timestamp { get; set; }

    [Description("The source of the log entry")]
    [JsonProperty("source")]
    public string Source { get; set; }

    [Description(
        "The details of the log entry, the content of which is determined by the service writing the log entry")]
    [JsonProperty("message")]
    public string Message { get; set; }
    }
}
