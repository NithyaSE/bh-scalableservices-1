﻿using System.Collections.Generic;
using System.Net.Http;
using BH.ScalableServices.Brokers.Shared.Models;

namespace BH.ScalableServices.Brokers.SDKBase
{
    public interface IScalableServiceWebClient
    {
        void PostLogEntry(ScalableServiceLogEntry logEntry, string runId);
        void PostLogEntries(ICollection<ScalableServiceLogEntry> logEntries, string runId);
        void UpdateStatus(object statusPatch, string runId);
        ScalableServiceRunDetails GetDetails(string runId,bool includeLogEntries=true);
    }
}
