﻿using System.Collections.Generic;
using System.Net.Http;
using BH.ScalableServices.Brokers. Shared.Models;

namespace BH.ScalableServices.Brokers.SDKBase
{
    public class FakeScalableServiceWebClient : IScalableServiceWebClient
    {
        public void PostLogEntry(ScalableServiceLogEntry logEntry, string runId)
        {
            return;
        }

        public void PostLogEntries(ICollection<ScalableServiceLogEntry> logEntries, string runId)
        {
            return;
        }

        public void UpdateStatus(object statusPatch, string runId)
        {
            return;
        }

        public ScalableServiceRunDetails GetDetails(string runId,bool includeLogEntries= true)
        {
            return new ScalableServiceRunDetails();
        }
    }

}
